<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id');
            $table->integer('parent_id');
            $table->string('title');
            $table->string('slug');
            $table->string('image')->nullable();
            $table->text('details')->nullable();
            $table->integer('show_in_menu')->default(0);
            $table->integer('status')->default(1);
            $table->text('meta_title')->nullable();            
            $table->text('meta_description')->nullable();            
            $table->text('meta_keyword')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
