@extends('layouts.app') 
@section('title') Thank You
@endsection
 
@section('content')

<div class="features py-5 gray-bg">
    <div class="container py-xl-5 py-lg-3">
        <div class="text-center mb-lg-5 mb-4">
            <h3 class="tittle mb-2">Thank You</h3>
        </div>
        <div class="row features-row">
            <div class="col-lg-12 mt-5  ">
                <p class="text-center">
                @if(request()->register)
                Thank you for your register.
                @elseif(request()->donation)
                Thank you for your donation.
                @else
                Thank you.
                @endif
                </p>
            </div>
        </div>

    </div>
</div>
@endsection