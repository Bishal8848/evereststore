@component('mail::message')
## Dear {{ $request['name'] }}

Thank you for joining us.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
