<div class="col-md-{{ isset($col) && $col? $col:3 }} product-men">
    <div class="men-pro-item simpleCart_shelfItem">
        <div class="men-thumb-item">
            <img src="{{ asset('uploads/product/'.$product->product_thumb) }}" alt="" width="150" height="150">
            <div class="men-cart-pro">
                <div class="inner-men-cart-pro">
                    {{-- <a href="{{ url($product->category->slug . '/' . $product->id) }}" class="link-product-add-cart">Quick View</a> --}}
                </div>
            </div>
            {{-- <span class="product-new-top">New</span> --}}
        </div>
        <div class="item-info-product ">
            <h4>
                <a href="#">
                    {{ $product->title }}
                </a>
            </h4>
            <div class="info-product-price">
                <span class="item_price">&pound;{{ $product->price }}</span>
                {{-- <del>$420.00</del> --}}
            </div>
            <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                <form action="{{ route('addtocart') }}" method="post">
                    <fieldset>
                        <input type="hidden" name="cmd" value="_cart" />
                        <input type="hidden" name="add" value="1" />
                        <input type="hidden" name="item_name" value="{{ $product->title }}" />
                        <input type="hidden" name="amount" value="{{ $product->price }}" />
                        <input type="hidden" name="currency_code" value="GBP">
                        <input type="submit" name="submit" value="Add to cart" class="button" />
                    </fieldset>
                </form>
            </div>

        </div>
    </div>
</div>