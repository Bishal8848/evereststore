    <div class="header-most-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <p>This is NOTICE BAR for short time Notice.</p>
                </div>
                <div class="col-sm-6">
                    <div class="store-btn">
                        <a class="everest-store" href="{{ url('/') }}">Everest Store</a>
                        <a class="happy-home" href="{{ url('happy-home') }}">Happy Home</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- //top-header -->   <!-- header-bot-->
    <div class="header-bot">
        <div class="header-bot_inner_textaninfo_header_mid">
            <!-- header-bot-->
            <div class="col-md-4 logo_contentmk">
                <h1>
                    @if($siteID == 1)
                        <a href="{{ url('/') }}">
                            <!--<span>G</span>rocery
                            <span>S</span>hoppy-->
                            <img src="{{ asset('images/logo2.png') }}" alt=" ">
                        </a>
                    @else
                        <a href="{{ url('happy-home') }}">
                            <!--<span>G</span>rocery
                            <span>S</span>hoppy-->
                            <img src="{{ asset('images/happyhome-logo.jpg') }}" alt=" " width="100">
                        </a>
                    @endif
                </h1>
            </div>
            <!-- header-bot -->
            <div class="col-md-8 header">
                <!-- header lists -->
                <ul>
                    {{-- <li>
                        <a class="play-icon popup-with-zoom-anim" href="#small-dialog1">
                            <span class="fa fa-map-marker" aria-hidden="true"></span> Shop Locator</a>
                    </li> --}}
                    <li class="visible-xs">
                        <form action="#" method="post">
                            <input type="hidden" name="cmd" value="_cart">
                            <input type="hidden" name="display" value="1">
                            <button type="submit" name="submit" value="">
                                <span class="fa fa-shopping-cart" aria-hidden="true"></span>0 Item(s) &pound;0.00
                            </button>
                        </form>

                    </li>
                    <li class="hidden-xs">
                        <a href="#" data-toggle="modal" data-target="#myModal1">
                            <span class="fa fa-truck" aria-hidden="true"></span>Track Order</a>
                    </li>
                    <li class="hidden-xs">
                        <span class="fa fa-envelope-o" aria-hidden="true"></span> sales@evereststore.co.uk
                    </li>
                    <li class="hidden-xs">
                        <span class="fa fa-phone" aria-hidden="true"></span> 0208 8549 289
                    </li>
                    @if(Auth::check())
                        <li class="dropdown myaccount">
                            <a class="genric-btn primary small circle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="">My Account [{{ Auth::user()->name }}]</span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a class="dropdown-item" href="{{ url('user') }}">Profile</a></li>
                                <li><a onclick="$('#logout').submit();return false;" class="dropdown-item" href="{{ route('logout') }}">Logout</a></li>
                                <form action="{{ route('logout') }}" method="post" id="logout">
                                    {!! csrf_field() !!}
                                </form>
                              </ul>
                        </li>
                    @else
                        <li>
                            <a href="#" data-toggle="modal" data-target="#myModal1">
                                <span class="fa fa-unlock-alt" aria-hidden="true"></span> Sign In </a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#myModal2">
                                <span class="fa fa-pencil-square-o" aria-hidden="true"></span> Sign Up </a>
                        </li>
                    @endif
                </ul>
                <!-- //header lists -->
                <!-- search -->
                <form action="{{ route('search') }}" method="get">
                    <div class="contentmkits_search row">
                        <div class="col-sm-4">
                            <select id="contentmkinfo-nav_search" name="category">
                                <option value="">All Categories</option>
                                @foreach($menu_categories as $category)
                                    @if($category->subcats()->count())
                                    <optgroup label="{{ $category->title }}">
                                        @foreach($category->subcats as $sub)
                                        <option value="{{ $sub->id }}" {{ request()->category && request()->category == $sub->id ? 'selected=selected' : null }}>{{ $sub->title }}</option>
                                        @endforeach
                                    </optgroup>
                                    @else
                                    <option value="{{ $category->id }}" {{ request()->category && request()->category == $category->id ? 'selected=selected' : null }}>{{ $category->title }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-8">
                            <input name="keyword" type="search" placeholder="How can we help you today?" required="" value="{{ request()->keyword ? : null }}">
                            <button type="submit" class="btn btn-default" aria-label="Left Align">
                                <span class="fa fa-search" aria-hidden="true"> </span>
                            </button>
                        </div>
                    </div>
                </form>
                <!-- //search -->
                <!-- cart details -->
                <div class="top_nav_right hidden-xs">
                    <div class="textancartaits textancartaits2 cart cart box_1">
                        <form action="#" method="post" class="last">
                            <input type="hidden" name="cmd" value="_cart">
                            <input type="hidden" name="display" value="1">
                            <button class="allahsuperview-cart" type="submit" name="submit" value="">
                                <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
                            </button>
                        </form>
                    </div>
                </div>
                <!-- //cart details -->
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //shop locator (popup) -->
    <!-- signin Model -->
    <!-- Modal1 -->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body modal-body-sub_contentmk">
                    <div class="main-mailposi">
                        <span class="fa fa-envelope-o" aria-hidden="true"></span>
                    </div>
                    <div class="modal_body_left modal_body_left1">
                        <h3 class="contentmkinfo_sign">Sign In </h3>
                        <p>
                            Sign In now, Let's start your Grocery Shopping. Don't have an account?
                            <a href="#" data-toggle="modal" data-target="#myModal2">
                                Sign Up Now</a>
                        </p>
                        <form action="{{ route('login') }}" method="post">
                            {!! csrf_field() !!}
                            <div class="styled-input contentmk-styled-input-top">
                                <input type="text" class="form-control" placeholder="E-mail" name="email">
                            </div>
                            <div class="styled-input">
                                <input type="password" placeholder="Password" name="password">
                            </div>
                            <input type="submit" value="Sign In">
                        </form>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- //Modal content-->
        </div>
    </div>
    <!-- //Modal1 -->
    <!-- //signin Model -->
    <!-- signup Model -->
    <!-- Modal2 -->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body modal-body-sub_contentmk">
                    <div class="main-mailposi">
                        <span class="fa fa-envelope-o" aria-hidden="true"></span>
                    </div>
                    <div class="modal_body_left modal_body_left1">
                        <h3 class="contentmkinfo_sign">Sign Up</h3>
                        <p>
                            Come join the Grocery Shoppy! Let's set up your Account.
                        </p>
                        <form action="{{ route('register') }}" method="post">
                            {{ csrf_field() }}
                            <div class="styled-input contentmk-styled-input-top">
                                <input type="text" placeholder="Name" name="name" required="">
                            </div>
                            <div class="styled-input">
                                <input type="email" placeholder="E-mail" name="email" required="">
                            </div>
                            <div class="styled-input">
                                <input type="password" placeholder="Password" name="password" id="password1" required="">
                            </div>
                            <div class="styled-input">
                                <input type="password" placeholder="Confirm Password" name="password_confirmation" id="password2" required="">
                            </div>
                            <input type="submit" value="Sign Up">
                        </form>
                        {{-- <p>
                            <a href="#">By clicking register, I agree to your terms</a>
                        </p> --}}
                    </div>
                </div>
            </div>
            <!-- //Modal content-->
        </div>
    </div>
    <!-- //Modal2 -->
    <!-- //signup Model -->
    <!-- //header-bot -->
    <!-- navigation -->
    <div class="ban-top">
        <div class="container">
            <div class="top_nav_left">
                <nav class="navbar navbar-default">
                    <div class="">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                                aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav menu__list">
                                <li><a class="nav-stylehead" href="{{ url('/') }}">Home</a></li>
                                @foreach($menu_categories as $cat)
                                    @if($cat->subcats()->count())
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle nav-stylehead" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $cat->title }}
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu multi-column columns-3">
                                            <div class="contentmk_inner_drop_nav_info">
                                                @php
                                                    $total = $cat->subcats()->count();
                                                    $col = 2;
                                                    $offset = 0;
                                                @endphp
                                                @for ($i = 1; $i <= $col; $i++)
                                                    @php
                                                        if($i>1){
                                                            $offset = $limit;
                                                        }
                                                        $limit = $offset + ceil($total/$col);
                                                    @endphp
                                                    <div class="col-sm-4 multi-gd-img">
                                                        <ul class="multi-column-dropdown">
                                                            @foreach($cat->subcats()->skip($offset)->limit($limit)->get() as $subcat)
                                                            <li><a href="{{ $subcat->url }}">{{ $subcat->title }}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endfor
                                                <div class="clearfix"></div>
                                            </div>
                                        </ul>
                                    </li>
                                    @else
                                    <li><a class="nav-stylehead" href="{{ $cat->url }}">{{ $cat->title }}</a></li>
                                    @endif
                                @endforeach
                                <li><a class="nav-stylehead" href="{{ url('contact') }}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>