    <!-- newsletter -->
    <div class="footer-top">
        <div class="container-fluid">
            <div class="col-xs-8 contentmk-leftmk">
                <h2>Get your Groceries delivered from local stores</h2>
                <p>Free Delivery on your first order!</p>
                <form action="#" method="post">
                    <input type="email" placeholder="E-mail" name="email" required="">
                    <input type="submit" value="Subscribe">
                </form>
                <div class="newsform-allahsuperl">
                    <span class="fa fa-envelope-o" aria-hidden="true"></span>
                </div>
            </div>
            <div class="col-xs-4 allahsuperl-rightmk">
                <img src="{{ asset('images/tab3.png') }}" alt=" ">
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //newsletter -->
    <!-- footer -->
    <footer>
        <div class="container">
            <!-- footer first section -->
            <p class="footer-main">
                <span>"Grocery Shoppy"</span> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
                magni dolores eos qui ratione voluptatem sequi nesciunt.Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
                beatae vitae dicta sunt explicabo.</p>
            <!-- //footer first section -->
            <!-- footer second section -->
            <div class="allahsuperl-grids-footer">
                <div class="col-xs-4 offer-footer">
                    <div class="col-xs-4 icon-fot">
                        <span class="fa fa-map-marker" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-8 text-form-footer">
                        <h3>{{ Label::ofValue('home:free_delivery') }}</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-xs-4 offer-footer">
                    <div class="col-xs-4 icon-fot">
                        <span class="fa fa-refresh" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-8 text-form-footer">
                        <h3>{{ Label::ofValue('home:save_time') }}</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-xs-4 offer-footer">
                    <div class="col-xs-4 icon-fot">
                        <span class="fa fa-times" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-8 text-form-footer">
                        <h3>{{ Label::ofValue('home:easy_to_cancel') }}</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- //footer second section -->
            <!-- footer third section -->
            <div class="footer-info allahsuper-contentmkits-info">
                <!-- footer categories -->
                <div class="col-sm-5 address-right">
                    <div class="col-xs-6 footer-grids">
                        <h3>Categories</h3>
                        <ul>
                            @foreach($menu_categories as $kitchen)
                            <li>
                                <a href="{{ url($kitchen->slug) }}">{{ $kitchen->title }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- //footer categories -->
                <!-- quick links -->
                <div class="col-sm-5 address-right">
                    <div class="col-xs-6 footer-grids">
                        <h3>Quick Links</h3>
                        <ul>
                            @foreach($footer_menu as $menu)
                                <li>
                                    <a href="{{ url($menu->slug) }}">{{ $menu->title }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xs-6 footer-grids">
                        <h3>Get in Touch</h3>
                        <ul>
                            <li>
                                <i class="fa fa-map-marker"></i>{{ Label::ofValue('home:get_in_touch_address') }}</li>
                            <li>
                                <i class="fa fa-mobile"></i> {{ Label::ofValue('home:get_in_touch_mobile') }} </li>
                            <li>
                                <i class="fa fa-phone"></i> {{ Label::ofValue('home:get_in_touch_phone') }} </li>
                            <li>
                                <i class="fa fa-envelope-o"></i>
                                <a href="mailto:{{ Label::ofValue('home:get_in_touch_email') }}"> {{ Label::ofValue('home:get_in_touch_email') }} </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- //quick links -->
                <!-- social icons -->
                <div class="col-sm-2 footer-grids  allahsuperl-socialmk">
                    <h3>Follow Us on</h3>
                    <div class="social">
                        <ul>
                            @foreach($socials as $social)
                                <li>
                                    <a class="icon fb" href="#">
                                        <i class="fa fa-{{ $social->icon }}"></i>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    {{-- <div class="contentmkits_app-devices">
                        <h5>Download the App</h5>
                        <a href="#">
                            <img src="images/1.png" alt="">
                        </a>
                        <a href="#">
                            <img src="images/2.png" alt="">
                        </a>
                        <div class="clearfix"> </div>
                    </div> --}}
                </div>
                <!-- //social icons -->
                <div class="clearfix"></div>
            </div>
            <!-- //footer third section -->
            <!-- footer fourth section (text) -->
            <div class="contentmk-sometext">
                @if($siteID==2)
                <div class="sub-some">
                    <h5>{{ Label::ofValue('home:welcome_hh') }}</h5>
                    <p>{{ Label::ofValue('home:welcome_hh_details') }}</p>
                </div>
                @else
                <div class="sub-some">
                    <h5>{{ Label::ofValue('home:wlecome_title') }}</h5>
                    <p>{{ Label::ofValue('home:welcome_details') }}</p>
                </div>
                @endif
                <div class="sub-some">
                    <h5>{{ Label::ofValue('home:shop_online') }}</h5>
                    <p>{{ Label::ofValue('home:shop_online_details') }}</p>
                </div>
                <div class="sub-some child-momu">
                    <h5>Payment Method</h5>
                    <ul>
                        <li>
                            <img src="images/pay2.png" alt="">
                        </li>
                        <li>
                            <img src="images/pay1.png" alt="">
                        </li>
                        <li>
                            <img src="images/paypal.jpg" alt="">
                        </li>
                        <li>
                            <img src="images/pay8.png" alt="">
                        </li>
                    </ul>
                </div>
                <!-- //payment -->
            </div>
            <!-- //footer fourth section (text) -->
        </div>
    </footer>
    <!-- //footer -->
    <!-- copyright -->
    <div class="copy-right">
        <div class="container">
            <div class="row">

                <div class="col-sm-6">
                    <p>© 2019 Everest Store  All rights reserved.</p>
                </div>
                <div class="col-sm-6">
                    <div class="developed-by">
                        <a href="http://w3web.co.uk"><p>Developed By: W3 Web Technology</p></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //copyright -->