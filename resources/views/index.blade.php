@extends('layouts.app')
@section('title','Home')

@section('meta')
<meta name="description" content="{{ $page->meta_description() }}">
<meta name="keywords" content="{{ $page->meta_keyword() }}"> @endsection

@section('content')
    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators-->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1" class=""></li>
            <li data-target="#myCarousel" data-slide-to="2" class=""></li>
            <li data-target="#myCarousel" data-slide-to="3" class=""></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>Big
                            <span>Save</span>
                        </h3>
                        <p>Get flat
                            <span>10%</span> Cashback</p>
                        <a class="button2" href="product.html">Shop Now </a>
                    </div>
                </div>
            </div>
            <div class="item item2">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>Healthy
                            <span>Saving</span>
                        </h3>
                        <p>Get Upto
                            <span>30%</span> Off</p>
                        <a class="button2" href="product.html">Shop Now </a>
                    </div>
                </div>
            </div>
            <div class="item item3">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>Big
                            <span>Deal</span>
                        </h3>
                        <p>Get Best Offer Upto
                            <span>20%</span>
                        </p>
                        <a class="button2" href="product.html">Shop Now </a>
                    </div>
                </div>
            </div>
            <div class="item item4">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>Today
                            <span>Discount</span>
                        </h3>
                        <p>Get Now
                            <span>40%</span> Discount</p>
                        <a class="button2" href="product.html">Shop Now </a>
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- //banner -->

    <!-- top Products -->
    <div class="ads-grid">
        <div class="container">
            <!-- tittle heading -->
            <h3 class="tittle-allahsuperl">Our Categories
                <span class="heading-style">
                    <i></i>
                    <i></i>
                    <i></i>
                </span>
            </h3>
            <div class="row">

                @foreach($categories as $cat)
                    <a class="col-sm-3" href="{{ $cat->url }}">
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <img src="{{ $cat->image('400x300') }}" alt="" class="img-responsive">
                                <br>
                                <h4>{{ $cat->title }}</h4>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
            <!-- //product right -->
        </div>
    </div>

    @if($offer)
    <div class="featured-section" id="projects">
        <div class="container">
            <!-- tittle heading -->
            <h3 class="tittle-allahsuperl">Festive Offers
                <span class="heading-style">
                    <i></i>
                    <i></i>
                    <i></i>
                </span>
            </h3>

            <div class="text-center">
                <h4>{!! $offer->title !!}</h4>
                <p>{!! $offer->details !!}</p>
            </div>
            <br>
            <!-- //tittle heading -->
            <div class="contentmkinfo-ads-display col-md-12">
                <div class="wrapper">
                    <div class="product-sec1">
                        @foreach($offer->products as $product)
                            @include('product-card')
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="ads-grid">
        <div class="container">
            <!-- tittle heading -->
            <h3 class="tittle-allahsuperl">Our Top Products
                <span class="heading-style">
                    <i></i>
                    <i></i>
                    <i></i>
                </span>
            </h3>
            <div class="contentmkinfo-ads-display col-md-12">
                <div class="wrapper">
                    <div class="product-sec1">
                        @foreach($top_products as $product)
                        {{-- {{ dd($product) }} --}}
                            <div class="col-md-3 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('uploads/product/'.$product->product_image) }}" alt="" width="150" height="150">
                                        {{-- <div class="men-cart-pro">
                                            <div class="inner-men-cart-pro">
                                                <a href="{{ url($product->category_slug . '/' . $product->id) }}" class="link-product-add-cart">Quick View</a>
                                            </div>
                                        </div> --}}
                                        {{-- <span class="product-new-top">New</span> --}}
                                    </div>
                                    <div class="item-info-product ">
                                        <h4>
                                            <a href="#">{{ $product->title }}</a>
                                        </h4>
                                        <div class="info-product-price">
                                            <span class="item_price">&pound;{{ $product->price }}</span>
                                            {{-- <del>$420.00</del> --}}
                                        </div>
                                        <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                            <form action="#" method="post">
                                                <fieldset>
                                                    <input type="hidden" name="cmd" value="_cart" />
                                                    <input type="hidden" name="add" value="1" />
                                                    <input type="hidden" name="item_name" value="{{ $product->title }}" />
                                                    <input type="hidden" name="amount" value="{{ $product->price }}" />
                                                    <input type="hidden" name="currency_code" value="GBP">
                                                    <input type="submit" name="submit" value="Add to cart" class="button" />
                                                </fieldset>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- //product right -->
        </div>
    </div>
    <!-- //top products -->
@endsection