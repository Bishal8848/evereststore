@extends('layouts.app') 
@section('title') {{ $page->m_title }}
@endsection
 
@section('content')

<section class="wthree-row w3-contact py-5 gray-bg">
	<div class="container py-xl-5 py-lg-3">
		<div class="text-center mb-lg-5 mb-4">
			<h3 class="tittle mb-2">{{ $page->title }}</h3>
			<p>Quick Send Us Message</p>
		</div>
		<div class="row contact-form py-3">
			<!-- contact form -->
			<div class="col-md-6 offset-md-3 wthree-form-left mt-lg-0 mt-5">
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				<hr> @endif @if (Session::has('success'))
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p>
						{{ Session::get('success') }}
					</p>
				</div>
				<hr> @endif

				<div class="contact-top1">
					<form action="{{ route('joinmail') }}" method="post" class="f-color">
						{!! csrf_field() !!}
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="contact-formw3ls form-control" name="name" id="name" required>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="contact-formw3ls form-control" name="email" id="email" required>
						</div>
						<div class="form-group">
							<label>Phone</label>
							<input type="tel" class="contact-formw3ls form-control" name="phone" id="phone" required>
						</div>
						<div class="form-group">
							<label>Your Message</label>
							<textarea class="contact-formw3ls form-control" rows="5" id="contactcomment" name="message" required></textarea>
						</div>
						<button type="submit" class="btn submit contact-submit">Submit</button>
					</form>
				</div>
			</div>
			<!-- //contact form -->
		</div>
	</div>
</section>
@endsection