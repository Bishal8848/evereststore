@extends('layouts.app') 
@section('title') Register
@endsection
 
@section('content')

<section class="wthree-row w3-contact py-5 gray-bg">
    <div class="container py-xl-5 py-lg-3">
        <div class="col-md-8 center-align">
            <div class="text-center mb-lg-5 mb-4">
                <h3 class="tittle mb-2">Register</h3>
                {{--
                <p>we'll update you how's the work of your money</p> --}}
            </div>
            <div class="row py-3">
                <p>
                    <b>Name:</b> {{$register->name}} <br>
                    <b>Email:</b> {{$register->email}} <br>
                    <b>Phone:</b> {{$register->phone}} <br>
                    <b>Price:</b> {{ $register->price ? '&pound;' . $register->price  : 'Free' }}<br>
                    <b>Program:</b> {{$register->program}}<br><br>

                    <b>message:</b><br>
                     {{$register->message}}<br>
                </p>
            </div>
            <hr>
            <div id="paypal-button"></div>
        </div>
        
    </div>
</section>
@endsection
 
@section('scripts')
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
    paypal.Button.render({
        env: "{{config('paypal.env')}}",
        client: {
            sandbox: "{{config('paypal.sandbox.client_id')}}",
            production: "{{config('paypal.production.client_id')}}"
        },

        style: {
          label: 'pay'
        },

        commit: true, // Show a 'Pay Now' button

        payment: function(data, actions) {
            var total = "{{ $register->price}}";
            return actions.payment.create({
                payment: {
                    transactions: [
                    {
                        amount: { total: total, currency: 'GBP' }
                    }
                    ]
                }
            });
        },

        onAuthorize: function(data, actions) {
            var url = "{!! url('register_paymentsuccess?_o=' .encrypt($register->id) .'=&_pid=')!!}";
            return actions.payment.execute().then(function(payment) {
                window.location.href = url + payment.id + '&payer_id=' + payment.payer.payer_info.payer_id;
            });
        }

    }, '#paypal-button');

</script>
@endsection