@extends('admin.app') 
@section('title') Donates
@endsection
 
@section('content')

<div class="content-table">
	@if ($donates->count() > 0)
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($donates as $donate)
				<tr>
					<td>{{ $donate->name }}</td>
					<td>{{ $donate->email }}</td>
					<td>{{ $donate->phone }}</td>
					<td>{{ $donate->created_at->format('M d, Y') }}</td>
					<td><a class="btn btn-info btn-sm" href="{{ route('donates.show', $donate->id) }}"><i class="fa fa-eye"></i></a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@else
	<p>Records not found</p>
	@endif
</div>
@endsection