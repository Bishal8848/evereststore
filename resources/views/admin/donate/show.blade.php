@extends('admin.app')

@section('title')
Donatation Details
@endsection

@section('content')

<h3 class="page-title">Donatation Details</h3>

<div class="panel">
	<div class="panel-heading">
        <h3 class="panel-title">Donatation Details</h3>
    </div>
	<div class="panel-body">		
		<p>
			<b>Name:</b> {{$donate->name}}<br>
			<b>Phone:</b> {{$donate->phone}}<br>
			<b>Email:</b> {{$donate->email}}<br>
			<b>Amount:</b> {{ $donate->price ? '&pound;' . $donate->price  : 'Free' }}<br>
			<b>Donation Type:</b> {{ $donate->type }}<br><br>
			
			<b>Message:</b> <br>
			{{ $donate->message }}
		</p>
	</div>	
</div>
@endsection

