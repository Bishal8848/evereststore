@extends('admin.app')
@section('title') Categories
@endsection

@section('content')

<div class="row">
	<div class="col-sm-6"><h3 class="page-title">Categories </h3></div>
	<div class="col-sm-4">
		<form action="" class="pull-right">
			<div class="form-group">
				<label class="col-sm-2 control-label" for="filter">Filter</label>
				<div class="col-sm-10">
					<select class="form-control" name="filter" id="filter" onchange="this.form.submit()">
						<option value="">All</option>
						<option value="es" {{ 'es' == request()->filter ? 'selected="selected"' : null }}>Main Category(EverstStore)</option>
						<option value="hh" {{ 'hh' == request()->filter ? 'selected="selected"' : null }}>Main Category(HappyHome)</option>
						@foreach ($parents as $parent)
						<option value="{{ $parent->id }}" {{ $parent->id == request()->filter ? 'selected="selected"' : null }}>{{ $parent->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</form>

	</div>
	<div class="col-sm-2"><a href="{{ route('categories.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></div>
</div>
<div class="panel">
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Store</th>
					<th>Parent</th>
					<th>Title</th>
					<th>Url</th>
					<th>Status</th>
					<th>Created At</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="sortable">
				@php $i=1;
				@endphp
				@foreach ($categories as $category)
				<tr id="{{ $category->id }}">
					<td>{{$i}}</td>
					<td>{{ $category->store_id ? config('store'.'.'.$category->store_id) : '-' }}</td>
					<td>{{ $category->parent ? $category->parent->title : 'None' }}</td>
					<td>{{ str_limit($category->title,30) }}</td>
					<td>{{ $category->slug == '/' ? '/' : '/' . str_limit($category->slug,30) }}</td>
					<td>
						@if ($category->status)
							<a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click to hide" href="{{ route('categories.showhide', $category->id) }}">
								<i class="lnr lnr-checkmark-circle"></i>
								Active
							</a>
						@else
							<a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click to show" href="{{ route('categories.showhide', $category->id) }}">
								<i class="lnr lnr-cross-circle"></i>
								Draft
							</a>
						@endif
					</td>
					<td>{{ $category->created_at->format('M d, Y') }}</td>
					<td class="text-right">

						<a class="btn btn-primary btn-sm" href="{{ route('categories.edit', $category->id) }}"><i class="fa fa-edit"></i></a>

						<div class="pull-right" style="margin-left: 10px;">
							<form onsubmit="return confirm('Are you sure?')" action="{{ route('categories.destroy', $category->id) }}" method="post">
								{{ method_field('DELETE') }} {{ csrf_field() }}
								<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
							</form>
						</div>
					</td>
				</tr>
				@php $i++;
				@endphp
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('script')
	@if(request()->filter)
	<script>
		$( function() {
			$( "#sortable" ).sortable({
				update: function (event, ui) {
					var data = $(this).sortable('toArray');
					url = "{{ route('categories.order') }}";
					$.ajax(url, {
						data: {
							'_token': "{{ csrf_token() }}",
							'data':data
						},
						type: 'POST',
					});
				}
			});
		} );

	</script>
	@endif
@endsection