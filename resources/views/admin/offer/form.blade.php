<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Title</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Title" name="title" value="{{ old('title', isset($offer) ? $offer->title : null) }}" type="text" id="title">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="8" placeholder="Details" name="details" id="details">{{ old('details', isset($offer) ? $offer->details : null) }}</textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="start_date">Start Date</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control datepicker" placeholder="Start Date" name="start_date" value="{{ old('start_date', isset($offer) ? $offer->start_date : null) }}" type="date" id="start_date">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="end_date">End Date</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control datepicker" placeholder="End Date" name="end_date" value="{{ old('end_date', isset($offer) ? $offer->end_date : null) }}" type="date" id="end_date">
	</div>
</div>

