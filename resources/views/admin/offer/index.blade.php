@extends('admin.app')

@section('title')
Offers
@endsection

@section('content')

<h3 class="page-title">Offers <a href="{{ route('offers.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-body">		
		<table class="table table-hover">
		<thead>
			<tr>
				<th>Title</th>
				<th>Details</th>
				<th>Start Date</th>
				<th>End Date</th>
				<th></th>
			</tr>
		</thead>
		<tbody id="sortable">
			@foreach ($offers as $offer)			

			<tr id="{{ $offer->id }}">
				<td>{{ $offer->title }}</td>
				<td>{!! $offer->details ? $offer->details : '-' !!}</td>
				<td>{{ $offer->start_date ? $offer->start_date : '-' }}</td>
				<td>{{ $offer->end_date ? $offer->end_date : '-' }}</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('offers.edit', $offer->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('offers.destroy', $offer->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection

