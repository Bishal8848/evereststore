@extends('admin.app') 
@section('title') Dashboard
@endsection
 
@section('content')
<h3 class="page-title">Dashboard</h3>

<div class="row">
    {{-- <div class="col-sm-12">
        <div class="panel panel-headline">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="fa fa-users fa-fw"></span> Latest register</h3>
            </div>
            <div class="panel-body">
                @if ($registers->count() > 0)
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Date</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($registers as $register)
                            <tr>
                                <td>{{$register->name}}</td>
                                <td>{{$register->email}}</td>
                                <td>{{$register->phone}}</td>
                                <td>{{$register->created_at->format('M d, Y')}}</td>
                                <td><a class="btn btn-info btn-sm" href="{{ route('registers.show', $register->id) }}"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <p>Records not found</p>
                @endif

            </div>
            
            <div class="panel-footer">
                <a href="{{ route('registers.index') }}" class="btn btn-primary">View all</a>
            </div>
        </div>
    </div> --}}
    <hr>

   {{--  <div class="col-sm-12">
        <div class="panel panel-headline">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="fa fa-money fa-fw"></span> Latest Donations</h3>
            </div>
            <div class="panel-body">
                @if ($donates->count() > 0)
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Date</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($donates as $donate)
                            <tr>
                                <td>{{$donate->name}}</td>
                                <td>{{$donate->email}}</td>
                                <td>{{$donate->phone}}</td>
                                <td>{{$donate->created_at->format('M d, Y')}}</td>
                                <td><a class="btn btn-info btn-sm" href="{{ route('donates.show', $donate->id) }}"><i class="fa fa-eye"></i></a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <p>Records not found</p>
                @endif

            </div>
            
            <div class="panel-footer">
                <a href="{{ route('donates.index') }}" class="btn btn-primary">View all</a>
            </div>
        </div>
    </div> --}}
</div>
@endsection