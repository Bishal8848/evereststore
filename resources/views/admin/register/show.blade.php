@extends('admin.app')

@section('title')
Register Details
@endsection

@section('content')

<h3 class="page-title">Register Details</h3>

<div class="panel">
	<div class="panel-heading">
        <h3 class="panel-title">Register Details</h3>
    </div>
	<div class="panel-body">		
		<p>
			<b>Name:</b> {{$register->name}}<br>
			<b>Phone:</b> {{$register->phone}}<br>
			<b>Email:</b> {{$register->email}}<br>
			<b>Program:</b> {{$register->program}}<br>
			<b>Price:</b> {{ $register->price ? '&pound;' . $register->price  : 'Free' }}<br><br>
			
			<b>Message:</b> <br>
			{{ $register->message }}
		</p>
	</div>	
</div>
@endsection

