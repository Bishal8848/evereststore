@extends('admin.app') 
@section('title') Registers
@endsection
 
@section('content')

<div class="content-table">
	@if ($registers->count() > 0)
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($registers as $register)
				<tr>
					<td>{{ $register->name }}</td>
					<td>{{ $register->email }}</td>
					<td>{{ $register->phone }}</td>
					<td>{{ $register->created_at->format('M d, Y') }}</td>
					<td><a class="btn btn-info btn-sm" href="{{ route('registers.show', $register->id) }}"><i class="fa fa-eye"></i></a>
						<div class="pull-right" style="margin-left: 10px;">
							<form onsubmit="return confirm('Are you sure?')" action="{{ route('registers.destroy', $register->id) }}" method="post">
								{{ method_field('DELETE') }} {{ csrf_field() }}
								<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
							</form>
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@else
	<p>Records not found</p>
	@endif
</div>
@endsection