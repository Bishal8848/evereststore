@extends('admin.app')

@section('title')
Brands
@endsection

@section('content')

<h3 class="page-title">Brands <a href="{{ route('brands.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-body">		
		<table class="table table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th>Logo</th>
				<th>Details</th>
				<th>Created At</th>
				<th></th>
			</tr>
		</thead>
		<tbody id="sortable">
			@foreach ($brands as $brand)			

			<tr id="{{ $brand->id }}">
				<td>{{ $brand->name }}</td>
				<td><img src="{{ asset('uploads/brand/'.$brand->logo) }}" width="50"></td>
				<td>{{ $brand->details ? $brand->details : '-' }}</td>
				<td>{{ $brand->created_at->format('M d, Y') }}</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('brands.edit', $brand->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('brands.destroy', $brand->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection

