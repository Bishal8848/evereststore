<div class="form-group">
	<label class="col-sm-2 control-label" for="category_id">Category</label>
	<div class="col-sm-3">
		<select class="form-control" name="category_id" id="category_id">
			<option value="0">None</option>
			@foreach ($categories as $category)
				@if($category->subcats()->count())
				<optgroup label="{{ $category->title }}">
					@foreach ($category->subcats as $sub)
						<option value="{{ $sub->id }}" {{ $sub->id == old('category_id', isset($product) ? $product->category_id : null ) ? 'selected="selected"' : null }}>{{ $sub->title }}</option>
					@endforeach
				</optgroup>
				@else
				<option value="{{ $category->id }}" {{ $category->id == old('category_id', isset($product) ? $product->category_id : null ) ? 'selected="selected"' : null }}>{{ $category->title }}</option>
				@endif
			@endforeach
		</select>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="brand_id">Brand</label>
	<div class="col-sm-3">
		<select class="form-control" name="brand_id" id="brand_id">
			<option value="0">None</option>
			@foreach ($brands as $brand)
			<option value="{{ $brand->id }}" {{ $brand->id == old('brand_id', isset($product) ? $product->brand_id : null ) ? 'selected="selected"' : null }}>{{ $brand->name }}</option>
			@endforeach
		</select>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="title">Title</label>
	<div class="col-sm-6">
		<input class="col-md-4 form-control" placeholder="Title" name="title" value="{{ old('title', isset($product) ? $product->title : null) }}" type="text" id="title">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="image">Image</label>
	<div class="col-sm-10">
		<input type="file" name="image" id="image">
		Image size: 1680X900px
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="details">Details</label>
	<div class="col-sm-10">
		<textarea class="col-md-8 form-control" rows="8" placeholder="Details" name="details" id="details">{{ old('details', isset($product) ? $product->details : null) }}</textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="features">Features</label>
	<div class="col-sm-6">
		<textarea class="col-md-8 form-control" rows="8" placeholder="Features" name="features" id="features">{{ old('features', isset($product) ? $product->features : null) }}</textarea>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="price">Price</label>
	<div class="col-sm-3">
		<input class="col-md-4 form-control" placeholder="Price" name="price" value="{{ old('price', isset($product) ? $product->price : null) }}" type="text" id="price">
	</div>

	<label class="col-sm-1 control-label">per</label>
	<div class="col-sm-2">
		<select class="form-control" name="unit_id" id="unit_id">
			<option value="0">None</option>
			@foreach ($units as $unit)
			<option value="{{ $unit->id }}" {{ $unit->id == old('unit_id', isset($product) ? $product->unit_id : null ) ? 'selected="selected"' : null }}>{{ $unit->title }}</option>
			@endforeach
		</select>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="special_price">Special Price</label>
	<div class="col-sm-3">
		<input class="col-md-4 form-control" placeholder="Special Price" name="special_price" value="{{ old('special_price', isset($product) ? $product->special_price : null) }}" type="text" id="special_price">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="inventory_status">Manage Stock</label>
	<div class="col-sm-3">
		<select class="form-control" name="inventory_status" id="inventory_status">
			<option value="1" {{ 1 == old('inventory_status', isset($product) ? $product->inventory_status : null ) ? 'selected="selected"' : null }}>Yes</option>
			<option value="0" {{ 0 == old('inventory_status', isset($product) ? $product->inventory_status : 0 ) ? 'selected="selected"' : null }}>No</option>
		</select>
	</div>
</div>

<div class="form-group" id="inventory_stock" style="display: none;">
	<label class="col-sm-2 control-label" for="inventory_stock">Inventory Stock</label>
	<div class="col-sm-3">
		<input class="col-md-4 form-control" placeholder="Inventory Stock" name="inventory_stock" value="{{ old('inventory_stock', isset($product) ? $product->inventory_stock : null) }}" type="number" id="inventory_stock">
	</div>
</div>

<div class="form-group">
	<div class="col-sm-3 col-sm-offset-2">
		<div class="checkbox">
			<label>
				<input type="checkbox" name="status" id="status" value="1" {{ old('status', isset($product) ? $product->status : 1) == 1 ? 'checked=checked' : null  }}> Active
			</label>
		</div>
	</div>

	<div class="col-sm-3">
		<div class="checkbox">
			<label>
				<input type="checkbox" name="top_product" class="top_product" id="top_product" value="1" {{ old('top_product', isset($product) ? $product->top_product : null) == 1 ? 'checked="checked"' : null }}> Top Product
			</label>
		</div>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="offers">Offers</label>
	@foreach($offers as $offer)
		<div class="col-sm-3">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="offers[]" class="type" id="offers" value="{{ $offer->id }}"
					{{ is_array(old('offers', isset($product) ? $product->offers()->allRelatedIds()->toArray():null)) && in_array($offer->id, old('offers', isset($product) ? $product->offers()->allRelatedIds()->toArray():null)) ? 'checked=checked': null }}>
					{{ $offer->title }}
				</label>
			</div>
		</div>
	@endforeach
</div>


@section('script')
	<script>
		$('#inventory_status').on('change', function() {
		  if(this.value == "1") {
		    $('#inventory_stock').show();
		  }
		  else{
		    $('#inventory_stock').hide();
		  }
		});

		$(function() {
		  if($('#inventory_status').val() == "1") {
		    $('#inventory_stock').show();
		  }
		  else{
		    $('#inventory_stock').hide();
		  }
		});
	</script>
@endsection