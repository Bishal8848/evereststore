@extends('admin.app')

@section('title')
Products
@endsection

@section('content')

<h3 class="page-title">Products <a href="{{ route('products.create') }}" class="btn btn-primary pull-right"><i class="fa fa-plus-circle"></i> <span>Add New</span></a></h3>

<div class="panel">
	<div class="panel-body">		
		<table class="table table-hover">
		<thead>
			<tr>
				<th>Image</th>
				<th>Title</th>
				<th>Category</th>
				<th>Brand</th>
				<th>Price / Per Unit</th>
				<th>Status</th>
				<th>Created At</th>
				<th></th>
			</tr>
		</thead>
		<tbody id="sortable">
			@foreach ($products as $product)			

			<tr id="{{ $product->id }}">
				<td><img src="{{ asset('uploads/product/'.$product->product_thumb) }}" width="50"></td>
				<td>{{ $product->title }}</td>
				<td>{{ $product->category_id ? $product->category->title : '-' }}</td>
				<td>{{ $product->brand_id ? $product->brand->name : '-' }}</td>
				<td>{{ $product->price }} / {{ $product->unit_id ? $product->unit->title : 'unit' }}</td>
				<td>
					@if ($product->status)
						<a class="text-success" data-toggle="tooltip" data-placement="bottom" title="Click to hide" href="{{ route('products.showhide', $product->id) }}">
							<i class="lnr lnr-checkmark-circle"></i>
							Active
						</a>
					@else
						<a class="text-danger" data-toggle="tooltip" data-placement="bottom" title="Click to show" href="{{ route('products.showhide', $product->id) }}">
							<i class="lnr lnr-cross-circle"></i>
							Draft
						</a>
					@endif 
				</td>
				<td>{{ $product->created_at->format('M d, Y') }}</td>
				<td class="text-right">					
					<a class="btn btn-primary btn-sm" href="{{ route('products.edit', $product->id) }}"><i class="lnr lnr-pencil"></i></a>
					<div class="pull-right" style="margin-left: 10px;"> 
						<form onsubmit="return confirm('Are you sure?')" action="{{ route('products.destroy', $product->id) }}" method="post">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-danger btn-sm"><i class="lnr lnr-trash"></i></button>
						</form>
					</div>
				</td>
			</tr>			
			@endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection

