@extends('layouts.app') 
@section('title') {{ $page->m_title }}
@endsection

@section('meta')
<meta name="description" content="{{ $page->meta_description() }}">
<meta name="keywords" content="{{ $page->meta_keyword() }}">
@endsection

@section('content')
<div class="services-breadcrumb">
    <div class="contentmk_inner_breadcrumb">
        <div class="container">
            <ul class="allahsuper_short">
                <li>
                    <a href="{{ url('/') }}">Home</a>
                    <i>|</i>
                </li>
                <li>{{ $page->title }}</li>
            </ul>
        </div>
    </div>
</div>

<div class="welcome">
        <div class="container">
            <!-- tittle heading -->
            <h3 class="tittle-allahsuperl">{{ $page->title }}
                <span class="heading-style">
                    <i></i>
                    <i></i>
                    <i></i>
                </span>
            </h3>
            <!-- //tittle heading -->
            <div class="allahsuperl-welcome-info">
                <div class="col-sm-6 col-xs-6 welcome-grids">
                    <div class="welcome-img">
                        <img src="images/about.jpg" class="img-responsive zoom-img" alt="">
                    </div>
                </div>
                <div class="col-sm-6 col-xs-6 welcome-grids">
                    <div class="welcome-img">
                        <img src="images/about2.jpg" class="img-responsive zoom-img" alt="">
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="allahsuperl-welcome-text">
                <p>{!! $page->details !!}</p>
            </div>
        </div>
    </div>
@endsection