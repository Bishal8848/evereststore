@extends('layouts.app') 
@section('title') Donate
@endsection
 
@section('content') {{--
<div class="features py-5 gray-bg">
    <div class="container py-xl-5 py-lg-3">
        <div class="text-center mb-lg-5 mb-4">
            <h3 class="tittle mb-2">{{ $page->title }}</h3>
        </div>
        <div class="row features-row">
            <div class="col-lg-12 mt-5 text-center ">
                {!! $page->details !!}
            </div>
        </div>

    </div>
</div> --}}

<section class="wthree-row w3-contact py-5 gray-bg">
    <div class="container py-xl-5 py-lg-3">
        <div class="col-md-8 center-align">
            <div class="text-center mb-lg-5 mb-4">
                <h3 class="tittle mb-2">Donate Now</h3>
                <p>we'll update you how's the work of your money</p>
            </div>
            <div class="row contact-form py-3">
                {{-- <h4>Donation Details</h4> --}}
                <p>
                    <b>Name:</b> {{$donate->name}} <br>
                    <b>Email:</b> {{$donate->email}} <br>
                    <b>Phone:</b> {{$donate->phone}} <br>
                    <b>Price:</b> &pound; {{$donate->price}}<br>
                    <br>
                    <b>Note:</b><br>
                    {{$donate->message}}
                </p>

            </div>
            <hr>
            <div id="paypal-button"></div>
        </div>

    </div>
</section>
@endsection
 
@section('scripts')
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
    paypal.Button.render({
        env: "{{config('paypal.env')}}",
        client: {
            sandbox: "{{config('paypal.sandbox.client_id')}}",
            production: "{{config('paypal.production.client_id')}}"
        },

        style: {
          size: 'responsive',
          // tagline: false,
          label: 'pay'
        },

        commit: true, // Show a 'Pay Now' button

        payment: function(data, actions) {
            var total = "{{$donate->price}}";
            return actions.payment.create({
                payment: {
                    transactions: [
                    {
                        amount: { total: total, currency: 'GBP' }
                    }
                    ]
                }
            });
        },

        onAuthorize: function(data, actions) {
            var url = "{!! url('donate_paymentsuccess?_o=' .encrypt($donate->id) .'=&_pid=')!!}";
            return actions.payment.execute().then(function(payment) {
                window.location.href = url + payment.id + '&payer_id=' + payment.payer.payer_info.payer_id;
            });
        }

    }, '#paypal-button');
</script>

@endsection