@extends('layouts.app') 
@section('title') {{ $page->m_title }}
@endsection
 
@section('content')
<div class="services-breadcrumb">
	<div class="contentmk_inner_breadcrumb">
		<div class="container">
			<ul class="allahsuper_short">
				<li>
					<a href="{{ url('/') }}">Home</a>
					<i>|</i>
				</li>
				<li>{{ $page->title }}</li>
			</ul>
		</div>
	</div>
</div>
<!-- //page -->
<!-- contact page -->
<div class="contact-allahsuperl">
	<div class="container">
		<!-- tittle heading -->
		<h3 class="tittle-allahsuperl">Contact Us
			<span class="heading-style">
				<i></i>
				<i></i>
				<i></i>
			</span>
		</h3>
		<!-- //tittle heading -->
		<!-- contact -->
		<div class="contact contentmkits">
			<div class="contact-contentmkinfo">
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				<hr> @endif @if (Session::has('success'))
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<p>
						{{ Session::get('success') }}
					</p>
				</div>
				<hr> @endif

				<div class="contact-form textan">
					<form action="{{ route('contactmail') }}" method="post" class="f-color">
						{!! csrf_field() !!}
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="contact-formw3ls form-control" name="name" id="name" required>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="contact-formw3ls form-control" name="email" id="email" required>
						</div>
						<div class="form-group">
							<label>Phone</label>
							<input type="tel" class="contact-formw3ls form-control" name="phone" id="phone" required>
						</div>
						<div class="form-group">
							<label>Your Message</label>
							<textarea class="contact-formw3ls form-control" rows="5" id="contactcomment" name="message" required></textarea>
						</div>
						<button type="submit" class="btn submit contact-submit">Submit</button>
					</form>
				</div>
				<div class="contact-right textan">
					<div class="col-xs-7 contact-text allahsuper-contentmkits">
						<h4>GET IN TOUCH :</h4>
						<p>
							<i class="fa fa-map-marker"></i> 123 Sebastian, NY 10002, USA.</p>
						<p>
							<i class="fa fa-phone"></i> Telephone : 333 222 3333</p>
						<p>
							<i class="fa fa-fax"></i> FAX : +1 888 888 4444</p>
						<p>
							<i class="fa fa-envelope-o"></i> Email :
							<a href="mailto:example@mail.com">mail@example.com</a>
						</p>
					</div>
					<div class="col-xs-5 contact-contentmk">
						<img src="images/contact2.jpg" alt="">
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<!-- //contact -->
	</div>
</div>
<!-- map -->
<div class="map mkstyles">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d55565170.29301636!2d-132.08532758867793!3d31.786060306224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1512365940398"
	    allowfullscreen></iframe>
</div>
@endsection