@extends('layouts.app')
@section('title','Home')

@section('content')
<div class="ads-grid">
    <div class="container">
        <!-- tittle heading -->
        <h3 class="tittle-allahsuperl">Search result for {{ $keyword }}
            <span class="heading-style">
                <i></i>
                <i></i>
                <i></i>
            </span>
        </h3>

        @if(count($products) > 0)
            <div class="contentmkinfo-ads-display col-md-12">
                <div class="wrapper">
                    <div class="product-sec1">
                        @foreach($products as $product)
                        {{-- {{ dd($product) }} --}}
                            <div class="col-md-4 product-men">
                                <div class="men-pro-item simpleCart_shelfItem">
                                    <div class="men-thumb-item">
                                        <img src="{{ asset('uploads/product/'.$product->product_thumb) }}" alt="" width="150" height="150">
                                        <div class="men-cart-pro">
                                            <div class="inner-men-cart-pro">
                                                <a href="{{ url($product->category->slug . '/' . $product->id) }}" class="link-product-add-cart">Quick View</a>
                                            </div>
                                        </div>
                                        {{-- <span class="product-new-top">New</span> --}}
                                    </div>
                                    <div class="item-info-product ">
                                        <h4>
                                            <a href="{{ url($product->category->slug . '/' . $product->id) }}">{{ $product->title }}</a>
                                        </h4>
                                        <div class="info-product-price">
                                            <span class="item_price">&pound;{{ $product->price }}</span>
                                            {{-- <del>$420.00</del> --}}
                                        </div>
                                        <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                            <form action="#" method="post">
                                                <fieldset>
                                                    <input type="hidden" name="cmd" value="_cart" />
                                                    <input type="hidden" name="add" value="1" />
                                                    <input type="hidden" name="business" value=" " />
                                                    <input type="hidden" name="item_name" value="Cashew Nuts, 100g" />
                                                    <input type="hidden" name="amount" value="200.00" />
                                                    <input type="hidden" name="discount_amount" value="1.00" />
                                                    <input type="hidden" name="currency_code" value="USD" />
                                                    <input type="hidden" name="return" value=" " />
                                                    <input type="hidden" name="cancel_return" value=" " />
                                                    <input type="submit" name="submit" value="Add to cart" class="button" />
                                                </fieldset>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        @else
            <h2>No Matching Results</h2>
        @endif
        <!-- //product right -->
    </div>
</div>
@endsection