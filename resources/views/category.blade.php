@extends('layouts.app')

@section('title',$category->title)

@section('content')
	<!-- banner-2 -->
	<div class="page-head_contentmk_info_allahsuperl">

	</div>
	<!-- //banner-2 -->
	<!-- page -->
	<div class="services-breadcrumb">
		<div class="contentmk_inner_breadcrumb">
			<div class="container">
				<ul class="allahsuper_short">
					<li>
						<a href="{{ url('/') }}">Home</a>
						<i>|</i>
					</li>
					<li>{{ $category->title }} Products</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- //page -->
	<!-- top Products -->
	<div class="ads-grid">
		<div class="container">
			<!-- tittle heading -->
			<h3 class="tittle-allahsuperl">{{ $category->title }} Products
				<span class="heading-style">
					<i></i>
					<i></i>
					<i></i>
				</span>
			</h3>
			<div class="side-bar col-md-3">
				<div class="">
					<h3>Categories</h3>
				<hr>
                <div>
                    <ul class="nav">
						@foreach ($menu_categories as $sidebar_cat)
							@if($sidebar_cat->subcats()->count())
							<li>
								<label label-default="" class="tree-toggle nav-header">{{ $sidebar_cat->title }}</label>
								<ul class="nav tree {{ $sidebar_cat->id == $category->id || $sidebar_cat->id == $category->parent_id ? 'open' : null }}">
									@foreach ($sidebar_cat->subcats as $sidebar_sub)
									<li><a href="{{ $sidebar_sub->url }}">{{ $sidebar_sub->title }}</a></li>
									@endforeach
								</ul>
							</li>
							@else
								<li><a href="{{ $sidebar_cat->url }}">{{ $sidebar_cat->title }}</a></li>
							@endif
						@endforeach
                    </ul>
                </div>
            </div>
			</div>
			<!-- product right -->
			<div class="contentmkinfo-ads-display col-md-9">
				<div class="wrapper">
					<!-- products section -->
					<div class="product-sec1">
						@foreach($products as $product)
						@include('product-card', ['col'=>4])
						@endforeach
						<div class="clearfix"></div>
					</div>
					{{ $products->links() }}
					<!-- //products section -->
				</div>
			</div>
			<!-- //product right -->
		</div>
	</div>
@endsection