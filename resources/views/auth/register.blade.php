@extends('layouts.app')
@section('title', 'Register')

@section('content')
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
        <div class="container">
            <div class="banner_content text-center">
                <h2>Register</h2>
            </div>
        </div>
    </div>
</section>

<div class="inner-page post">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 offset-sm-4">

                <div class="text-center py-5">

                    <div class="card">
                        <div class="card-header pt-4">
                            <h5>Register</h5>
                        </div>
                      <div class="card-body py-4">

                        @if ($errors->any())
                            <div class="alert alert-danger text-left">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form method="post" action="{{ route('register') }}" autocomplete="off">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="E-mail">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>

                            <div class="form-group">
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Re-enter Password">
                            </div>

                            <div class="form-group">
                                <button class="genric-btn info radius small">Join Now</button>
                            </div>

                        </form>
                      </div>
                      <div class="card-footer text-muted"">
                        <p>Already have an account? <a href="{{ route('login') }}">Login</a></p>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection