@extends('layouts.app')
@section('title', 'Login')

@section('content')
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
        <div class="container">
            <div class="banner_content text-center">
                <h2>Login</h2>
            </div>
        </div>
    </div>
</section>

<div class="inner-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">

                <div class="text-center">

                    <div class="card">
                        {{-- <div class="card-header pt-4">
                            <h5>Welcome Back, Please login</h5>
                        </div> --}}
                        <br>
                      <div class="card-body py-4">

                        @if ($errors->any())
                            <div class="alert alert-danger text-left">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('login') }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="E-mail" name="email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password" name="password">
                            </div>

                            <div class="form-group">
                                <button class="genric-btn info radius small">Login</button>
                            </div>
                        </form>
                      </div>
                      <div class="card-footer text-muted"">
                        <p>Don't have an account? <a href="#" data-toggle="modal" data-target="#myModal2">Sign Up</a></p>
                        <p><a href="#">Forgot your password?</a></p>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection