@extends('layouts.app')
@section('title', 'Change Password')

@section('content')
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
        <div class="container">
            <div class="banner_content text-center">
                <h2>Change Password</h2>
            </div>
        </div>
    </div>
</section>

<div class="inner-page post">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 offset-sm-4">

                <div class="text-center py-5">

                    <div class="card">
                        <div class="card-header pt-4">
                            <h5>Change Password</h5>
                        </div>
                      <div class="card-body py-4">

                        @if ($errors->any())
                            <div class="alert alert-danger text-left">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" method="POST" action="{{ route('password.change') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                                        <label for="old_password" class="col-md-4 control-label">Password</label>

                                        <div class="col-md-6">
                                            <input id="old_password" type="password" class="form-control" name="old_password" required>

                                            @if ($errors->has('old_password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('old_password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 control-label">New Password</label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                change Password
                                            </button>
                                        </div>
                                    </div>
                                </form>
                      </div>
                      <div class="card-footer text-muted"">
                        <p>Don't have an account? <a href="{{ route('register') }}">Sign Up</a></p>
                        <p><a href="{{ route('password.request') }}">Forgot your password?</a></p>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection