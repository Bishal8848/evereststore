@extends('layouts.app')
@section('title', 'Reset Password')

@section('content')
<section class="banner_area">
    <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
        <div class="container">
            <div class="banner_content text-center">
                <h2>Reset Password</h2>
            </div>
        </div>
    </div>
</section>

<div class="inner-page post">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 offset-sm-4">

                <div class="text-center py-5">

                    <div class="card">
                        <div class="card-header pt-4">
                            <h5>Reset Password</h5>
                        </div>
                      <div class="card-body py-4">

                        @if ($errors->any())
                            <div class="alert alert-danger text-left">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p>
                                    {{ Session::get('success') }}
                                </p>
                            </div>
                        @endif

                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="control-label">E-Mail Address</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span> @endif
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Send Password Reset Link
                                        </button>
                                    </div>
                                </div>
                            </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection