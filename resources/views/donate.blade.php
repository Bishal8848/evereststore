@extends('layouts.app') 
@section('title') Donate
@endsection
 
@section('content') {{--
<div class="features py-5 gray-bg">
    <div class="container py-xl-5 py-lg-3">
        <div class="text-center mb-lg-5 mb-4">
            <h3 class="tittle mb-2">{{ $page->title }}</h3>
        </div>
        <div class="row features-row">
            <div class="col-lg-12 mt-5 text-center ">
                {!! $page->details !!}
            </div>
        </div>

    </div>
</div> --}}

<section class="wthree-row w3-contact py-5 gray-bg">
    <div class="container py-xl-5 py-lg-3">
        <div class="col-md-8 center-align">
            <div class="text-center mb-lg-5 mb-4">
                <h3 class="tittle mb-2">Donate US</h3>
                <p>we'll update you how's the work of your money</p>
            </div>
            <div class="row contact-form py-3">
                <!-- donation form -->
                <form action="{{ route('donate') }}" style="width: 100%;" method="post">
                    {!! csrf_field() !!}
                    <div class="col-lg-12 wthree-form-left mt-lg-0 mt-5">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        <div class="form-group row" id="type_btns">
                            <div class="col-sm">
                                <button type="button" class="button button-caution button-large type_btn button-block" value="once">Once</button>
                            </div>
                            <div class="col-sm">
                                <button type="button" class="button button-plain button-large type_btn button-block" value="monthly">Monthly</button>
                            </div>

                            <input type="hidden" name="donate_type" id="donate_type" value="once" />
                        </div>
                        {{--
                        <h3 class="donate-heading">Select an amount </h3> --}}
                        <div class="form-group row" id="ant_btns">
                            <div class="col-sm">
                                <button type="button" class="button button-primary button-block amt_btn" value="5">&pound; 5</button>
                            </div>
                            <div class="col-sm">
                                <button type="button" class="button button-plain button-block amt_btn" value="10">&pound; 10</button>
                            </div>
                            <div class="col-sm">
                                <button type="button" class="button button-plain button-block amt_btn" value="20">&pound; 20</button>
                            </div>
                            <div class="col-sm">
                                <button type="button" class="button button-plain  button-block amt_btn" value="50">&pound; 50</button>
                            </div>
                            <div class="col-sm">
                                <button type="button" class="button button-plain button-block amt_btn" value="100">&pound; 100</button>
                            </div>
                            <div class="col-sm">
                                <button type="button" class="button button-plain button-block amt_btn" value="other">Other</button>
                            </div>
                            <input type="hidden" name="price" id="price" value="5" />
                        </div>

                        <div class="form-group" style="display:none" id="other_amt">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">&pound;</span>
                                </div>
                                <input type="text" class="form-control" name="other_price" id="other_price">
                                <div class="input-group-append">
                                    <span class="input-group-text">.00</span>
                                </div>
                            </div>
                        </div>

                        <h3 class="donate-heading">Your Information </h3>

                        <div class="contact-top1">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Frist Name</label>
                                    <input type="text" class="contact-formw3ls form-control" id="first_name" name="first_name" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Last Name</label>
                                    <input type="text" class="contact-formw3ls form-control" id="last_name" name="last_name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="contact-formw3ls form-control" id="email" name="email" required>
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="tel" class="contact-formw3ls form-control" id="phone" name="phone" required>
                            </div>
                            <div class="form-group">
                                <label for="inputState">Gender</label>
                                <select id="inputState" class="form-control">
                                <option selected>Choose...</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                                <option value="other">Other</option>
                            </select>
                            </div>

                            <div class="form-group">
                                <label>Your Message</label>
                                <textarea class="contact-formw3ls form-control" rows="5" id="message" name="message" required></textarea>
                            </div>
                            <button type="submit" class="btn submit contact-submit">Donate Now</button>
                        </div>
                    </div>
                </form>
                <!-- //contact form -->
            </div>
        </div>

    </div>
</section>
@endsection
 
@section('scripts')
<script>
    $(document).on('click', '.amt_btn', function(){
        $('#ant_btns').find('.button-primary').removeClass('button-primary').addClass('button-plain');
        $(this).addClass('button-primary').removeClass('button-plain');
        if($(this).val() == 'other'){
            $('#other_amt').show();
        }else{
            $('#other_amt').hide();
            $('#price').val($(this).val());
            $('#other_price').val(0);
        }
    });

    $(document).on('click', '.type_btn', function(){
        $('#type_btns').find('.button-caution').removeClass('button-caution').addClass('button-plain');
        $(this).addClass('button-caution').removeClass('button-plain');
        $('#donate_type').val($(this).val());
    });

</script>
@endsection