<?php

Route::get('/', 'PageController@home')->name('_root_');
Route::get('happy-home', 'PageController@home');


// routes for admin
Route::get('admin/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Admin\LoginController@login');
Route::get('admin/logout', 'Admin\LoginController@logout')->name('admin.logout');
Route::post('admin/logout', 'Admin\LoginController@logout');
Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'namespace' => 'Admin'], function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::get('/phpinfo', 'DashboardController@phpinfo');
    Route::get('change-password','DashboardController@changePassword')->name('change.password');
    Route::post('change-password','DashboardController@changePassword');
    Route::resource('pages', 'PageController');
    Route::post('pages/order', 'PageController@order')->name('pages.order');
    Route::get('pages/showhide/{page}', 'PageController@showhide')->name('pages.showhide');
    Route::resource('slides', 'SlideController');
    Route::post('slides/order', 'SlideController@order')->name('slides.order');
    Route::get('slides/showhide/{slide}', 'SlideController@showhide')->name('slides.showhide');
    Route::resource('labels', 'LabelController');
    Route::resource('settings', 'SettingController');
    Route::resource('reviews', 'ReviewController');
    Route::get('reviews/approve/{review}', 'ReviewController@approve')->name('reviews.approve');
    Route::get('reviews/cancel/{review}', 'ReviewController@cancel')->name('reviews.cancel');
    Route::resource('galleries', 'GalleryController');
    Route::resource('socials', 'SocialController');
    Route::resource('admins', 'AdminController');
    Route::resource('registers', 'RegisterController');
    Route::resource('donates', 'DonateController');
    Route::resource('categories', 'CategoryController');
    Route::post('categories/order', 'CategoryController@order')->name('categories.order');
    Route::get('categories/showhide/{category}', 'CategoryController@showhide')->name('categories.showhide');
    Route::resource('units', 'UnitController');
    Route::resource('brands', 'BrandController');
    Route::resource('products', 'ProductController');
    Route::get('products/showhide/{product}', 'ProductController@showhide')->name('products.showhide');
    Route::resource('offers', 'OfferController');
});

Route::group(['middleware' => ['auth'], 'namespace' => 'User'], function () {
    Route::get('user', 'UserController@profile')->name('user.profile');
});

Auth::routes();

$router = app()->make('router');
$pages = App\Page::all();
foreach ($pages as $page) {
    if ($page->slug == 'contact') {
        $router->get($page->slug, 'PageController@contact');
        $router->post($page->slug, 'PageController@contactmail')->name('contactmail');
    } elseif ($page->slug == 'reviews') {
        $router->get($page->slug, 'PageController@review');
    } elseif ($page->slug == 'join-with-us') {
        $router->get($page->slug, 'PageController@join');
        $router->post($page->slug, 'PageController@joinmail')->name('joinmail');
    } elseif ($page->slug != '/') {
        $router->get($page->slug, 'PageController@index');
    }
}


$cats = App\Category::all();
foreach ($cats as $page) {
    if($page->store_id == 2){
        $router->get('happy-home/'.$page->slug, 'CategoryController@index');        
    }else{
        $router->get($page->slug, 'CategoryController@index');
    }
}

Route::get(\App\Site::ID() . 'category/{category}', 'CategoryController@category')->name('category');
Route::get(\App\Site::ID() . 'category/{category}/{product}', 'CategoryController@product')->name('category.product');

Route::get('search', 'SearchController@index')->name('search');
Route::get('{category}/{product}', 'ProductController@product')->name('productdetails');

Route::post('addtocart', 'CartController@addtocart')->name('addtocart');


Route::post('reviews', 'PageController@reviewsave')->name('review');


Route::get('donate', 'PageController@donate')->name('donate');
Route::post('donate', 'PageController@donate_save');
Route::get('donate/{donate}', 'PageController@donate_payment')->name('donate_payment');
Route::get('donate_paymentsuccess', 'PageController@donate_paymentsuccess')->name('donate_paymentsuccess');


Route::get('thankyou', 'PageController@thankyou')->name('thankyou');

//cart
Route::post('addtocart', 'CartController@addtocart')->name('addtocart');


//Preview email on brower
Route::get('emailview/{id}', function ($id) {
    $tablebook = \App\Order::find($id);
    return new \App\Mail\NewOrder($tablebook);
});
