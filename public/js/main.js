jQuery(function($) {
    /*--------------------------------------------------------------
    #Sticky Header
    --------------------------------------------------------------*/
    if ($(window).width() > 1) {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 1) {
                $('.site-header').addClass("sticky-header");
                $('body').addClass('sticky');
            } else {
                $('.site-header').removeClass("sticky-header");
                $('body').removeClass('sticky');
            }
        });
    }
    /*--------------------------------------------------------------
    slick
    --------------------------------------------------------------*/
    $('.slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 6000,
            prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
            nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>'
    });

    $('.reviewslide').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 6000,
            prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
            nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>'
    });


    /*--------------------------------------------------------------
      back to top
      --------------------------------------------------------------*/
    $('body').prepend('<a href="#" class="back-to-top">Top</a>');
    var amountScrolled = 100;
    $(window).scroll(function() {
        if ($(window).scrollTop() > amountScrolled) {
            $('a.back-to-top').fadeIn('slow');
        } else {
            $('a.back-to-top').fadeOut('slow');
        }
    });
    $('a.back-to-top, a.simple-back-to-top').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 700);
        return false;
    });


    /*Mobile Mebu*/
     $('#nav-icon').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('menu-opened');
        $('.main-navigation').toggleClass('menu-open');
    });


});