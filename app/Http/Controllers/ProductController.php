<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;

class ProductController extends Controller
{
    public function product($category, Product $product)
    {
        if($product && $product)
    	return view('single-product', compact('category','product'));
    }
}
