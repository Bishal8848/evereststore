<?php

// ALTER TABLE `registers` ADD `program` TEXT NULL AFTER `message`, ADD `price` FLOAT(8,2) NULL AFTER `program`;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Slide;
use App\Product;

use App\Review;
use App\Donate;
use App\Register;

use Mail;
use App\Mail\DonateNow;
use App\Mail\ThankYouForDonation;
use App\Mail\SpaceRegister;
use App\Mail\Contact;
use App\Mail\JoinUs;
use App\Site;
use DB;
use App\Category;
use App\Offer;

class PageController extends Controller
{
    public function home()
    {
        $page = Page::find(1);
        if (!$page) {
            $page = Page::where('slug', '/')->first();
        }
        $data['page'] = $page;
        $data['offer'] = Offer::find(2);
        $data['categories'] = \App\Category::where('store_id', \App\Site::ID())->where('parent_id', 0)->orderBy('order_by')->get();


        // $data['top_products'] = DB::table('products')
        //     ->join('categories', 'products.category_id', '=', 'categories.id')
        //     ->join('images', 'products.id', '=', 'images.product_id')
        //     ->where('products.top_product', 1)
        //     ->where('categories.store_id', Site::ID())
        //     ->select(
        //         'products.*',
        //         'images.image as product_image',
        //         'categories.slug as category_slug'
        //     )
        //     ->get();

        $data['top_products'] = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->join('images', 'products.id', '=', 'images.product_id')
            ->where('products.top_product', 1)
            ->where('categories.store_id', Site::ID())
            ->select(
                'products.*',
                'images.image as product_image',
                'categories.slug as category_slug'
            )
            ->get();
        // dd($data['top_products']);
        return view('index', $data);
    }

    public function index(Request $request)
    {
        $data['page'] = $page =  Page::where('slug', $request->path())->first();
        return view('page', $data);
    }

    public function contact(Request $request)
    {
        $data['page'] = $page =  Page::where('slug', $request->path())->first();
        return view('contact', $data);
    }

    public function contactmail(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required',
        ]);
        Mail::send(new Contact($request));
        return redirect()->route('thankyou');
    }

    public function join(Request $request)
    {
        $data['page'] = $page =  Page::where('slug', $request->path())->first();
        return view('join', $data);
    }

    public function joinmail(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required',
        ]);
        Mail::send(new JoinUs($request));
        return redirect()->route('thankyou');
    }

    public function donate()
    {
        return view('donate');
    }

    public function donate_save(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
        ]);

        $donate = new Donate;
        $donate->first_name = $request->first_name;
        $donate->last_name = $request->last_name;
        $donate->email = $request->email;
        $donate->phone = $request->phone;
        $donate->message = $request->message;
        $donate->price = $request->other_price ?: $request->price;
        $donate->type = $request->donate_type;
        $donate->save();
        return redirect()->route('donate_payment', ['donate' => $donate->id]);
    }

    public function donate_payment(Donate $donate)
    {
        return view('donate_payment', compact('donate'));
    }

    public function donate_paymentsuccess(Request $request)
    {
        $donate = decrypt($request->_o);
        $donate = Donate::find($donate);
        $donate->status = 1;
        $donate->save();
        Mail::send(new DonateNow($donate));
        Mail::send(new ThankYouForDonation($donate));
        return redirect()->route('thankyou', ['register' => 1]);
    }


    public function space_register(Request $request)
    {
        $register = new Register;
        $register->first_name = $request->first_name;
        $register->last_name = $request->last_name;
        $register->email = $request->email;
        $register->phone = $request->phone;
        $register->message = $request->message;
        $register->program = $request->program_title;
        $register->price = $request->program_price == 'free' ? 0 : $request->program_price;
        $register->save();

        if ($register->price <> 'free') {
            return redirect()->route('register_payment', ['id' => $register->id]);
        }

        Mail::send(new SpaceRegister($register));
        return redirect()->route('thankyou', ['register' => 1]);
    }

    public function register_payment(Register $register)
    {
        return view('register_payment', compact('register'));
    }

    public function register_paymentsuccess(Request $request)
    {
        $register = decrypt($request->_o);
        $register = Register::find($register);
        $register->status = 1;
        $register->save();
        Mail::send(new SpaceRegister($register));
        return redirect()->route('thankyou', ['register' => 1]);
    }

    public function thankyou()
    {
        return view('thankyou');
    }


    public function review(Request $request)
    {
        $data['page'] = $page =  Page::where('slug', $request->path())->first();
        $data['reviews'] = Review::active()->get();
        return view('review', $data);
    }

    public function reviewsave(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'image' => 'image|max:1024',
            'review' => 'required',
        ]);

        $review = new review;
        $review->name = $request->name;
        $review->email = $request->email;
        $review->details = $request->review;
        $review->status = 0;
        $review->save();

        if ($request->hasFile('image')) {
            $image_name = 'review-' . $review->id . '.' . $request->image->extension();
            $path = $request->image->move('uploads/reviews/', $image_name);
            $review->image = $image_name;
            $review->save();
        }

        return redirect(url('reviews'))->with('success', 'thank you');
    }
}
