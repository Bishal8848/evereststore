<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product;

class CartController extends Controller
{
    public function addtocart(Request $request)
    {
    	dd($request);
        $product = Product::find($request->product_id);

        $carts = Cart::search(function ($cartproduct, $rowId) use ($product) {
            return $cartproduct->id === $product->id;
        });
        if (count($carts) == 0) {
            Cart::add($product->id, $product->title, $request->quantity, $product->price);
        } else {
            foreach ($carts as $cart) {
                Cart::update($cart->rowId, ($cart->qty + $request->quantity));
            }
        }
        if ($request->ajax()) {
            $data['status'] = 'success';
            $data['message'] = '1 product added to cart';
            return $data;
        }
        return redirect()->back();
    }
}
