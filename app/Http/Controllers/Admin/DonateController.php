<?php

namespace App\Http\Controllers\Admin;

use App\Donate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DonateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $donates = Donate::latest()->get();
        return view('admin.donate.index', compact('donates'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Donate  $donate
     * @return \Illuminate\Http\Response
     */
    public function show(Donate $donate)
    {
        return view('admin.donate.show', compact('donate'));
    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Donate  $donate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Donate $donate)
    {
        if($donate && $donate->delete()){
            return redirect()->route('donates.index')->with('success', 'Donate deleted.');
        }else{
            return redirect()->route('donates.index')->with('error', 'Error while deleting Donate.');
        }
    }
}
