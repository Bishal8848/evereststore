<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::all();
        return view('admin.brand.index',compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'logo' => 'max:2048|mimes:jpg,png,jpeg',
        ]);

        $data = array(
            'name' => $request->name,
            'slug' => str_slug($request->name,'-'),
            'details' => $request->details,
        );

        $brand = Brand::create($data);
        if($request->hasFile('logo'))
        {
            $image_name = $brand->name.'-'.$brand->id.'.'.$request->logo->extension();
            $path = $request->logo->move('uploads/brand/',$image_name);
            $brand->logo = $image_name;
            $brand->save();
        }
        return redirect()->route('brands.index')->with('success','Brand Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('admin.brand.edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $this->validate($request, [
            'name' => 'required',
            'logo' => 'max:2048|mimes:jpg,png,jpeg',
        ]);

        $data = array(
            'name' => $request->name,
            'slug' => str_slug($request->name,'-'),
            'details' => $request->details,
        );

        Brand::where('id',$brand->id)->update($data);
        if($request->hasFile('logo'))
        {
            if($brand->logo && file_exists(public_path('uploads/brand/'.$brand->logo)))
            {
                unlink(public_path('uploads/brand/'.$brand->logo));
            }
            $image_name = $brand->name.'-'.$brand->id.'.'.$request->logo->extension();
            $path = $request->logo->move('uploads/brand/',$image_name);
            $brand->logo = $image_name;
            $brand->save();
        }
        return redirect()->route('brands.index')->with('success','Brand Saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        if($brand){

            if($brand->logo){
                $logo = public_path('uploads/brand/' . $brand->logo);
                if(file_exists($logo)){
                    unlink($logo);
                }
            }

            if($brand->delete()){
                return redirect()->route('brands.index')->with('success', 'Brand deleted.');
            }else{
                return redirect()->route('brands.index')->with('error', 'Error while deleting brand.');
            }

        }else{
            abort();
        }
    }
}
