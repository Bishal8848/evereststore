<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::query();
        if ($request->filter && is_numeric($request->filter)) {
            $categories->where('parent_id', $request->filter);
        } elseif ($request->filter) {
            $categories->where('parent_id', 0);
            $categories->where('store_id', $request->filter == 'es' ?: 2);
        }

        if ($request->filter) {
            $categories->orderBy('order_by');
        }
        $categories = $categories->get();
        $parents = Category::where('parent_id', 0)->get();
        return view('admin.category.index', compact('categories', 'parents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'store_id' => 'required',
            'parent_id' => 'required',
            'title' => 'required',
        ]);

        $data = array(
            'store_id' => $request->store_id,
            'parent_id' => $request->parent_id,
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'details' => $request->details,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
            'meta_keyword' => $request->meta_keyword,
        );

        $category = Category::create($data);

        if ($request->hasFile('image')) {
            $image_name = $category->title . '-' . $category->id . '.' . $request->image->extension();
            $path = $request->image->move('uploads/category/', $image_name);
            $category->image = $image_name;
            $category->save();
        }
        return redirect()->route('categories.index')->with('success', 'Category Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::all();
        return view('admin.category.edit', compact('categories', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'store_id' => 'required',
            'parent_id' => 'required',
            'title' => 'required',
        ]);

        if ($request->parent_id) {
            $order_by = Category::where('parent_id', $request->parent_id)->max('order_by') + 1;
        } else {
            $order_by = Category::where('store_id', $request->store_id)->max('order_by') + 1;
        }


        $data = array(
            'store_id' => $request->store_id,
            'parent_id' => $request->parent_id,
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'details' => $request->details,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
            'meta_keyword' => $request->meta_keyword,
            'order_by' => $order_by,
        );

        Category::where('id', $category->id)->update($data);

        if ($request->hasFile('image')) {
            if ($category->image && file_exists(public_path('uploads/category/' . $category->image))) {
                unlink(public_path('uploads/category/' . $category->image));
            }
            $image_name = $request->title . '-' . $category->id . '.' . $request->image->extension();
            $path = $request->image->move('uploads/category/', $image_name);
            $category->image = $image_name;
            $category->save();
        }

        return redirect()->route('categories.index')->with('success', 'Category Saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category) {

            if ($category->image) {
                $image = public_path('uploads/category/' . $category->image);
                if (file_exists($image)) {
                    unlink($image);
                }
            }

            if ($category->delete()) {
                return redirect()->route('categories.index')->with('success', 'category deleted.');
            } else {
                return redirect()->route('categories.index')->with('error', 'Error while deleting category.');
            }
        } else {
            abort(404);
        }
    }

    public function showhide(Category $category)
    {
        if ($category->status) {
            $category->status = 0;
        } else {
            $category->status = 1;
        }
        $category->save();
        return redirect()->route('categories.index')->with('success', 'Status Updated.');
    }

    public function order(Request $request)
    {
        foreach ($request->data as $odr => $id) {
            if ($id) {
                $odr = $odr + 1;
                $slide = Category::find($id);
                $slide->order_by = $odr;
                $slide->save();
            }
        }
    }
}
