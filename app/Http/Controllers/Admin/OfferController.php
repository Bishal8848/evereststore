<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Offer;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = Offer::all();
        return view('admin.offer.index',compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.offer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [ 
            'title' => 'required',
        ]);

        $data = array(
            'title' => $request->title,
            'details' => $request->details,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        );

        Offer::create($data);
        return redirect()->route('offers.index')->with('success','Offer Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        return view('admin.offer.edit',compact('offer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        $this->validate($request, [ 
            'title' => 'required',
        ]);

        $data = array(
            'title' => $request->title,
            'details' => $request->details,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        );

        Offer::where('id',$offer->id)->update($data);
        return redirect()->route('offers.index')->with('success','Offer Saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        if($offer && $offer->delete()){
            return redirect()->route('offers.index')->with('success', 'Offer Deleted.');
        }else{
            return redirect()->route('offers.index')->with('error', 'Error while deleting offer.');
        }
    }
}
