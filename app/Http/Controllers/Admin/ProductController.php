<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Unit;
use App\Image;
use App\Brand;
use App\Offer;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['products'] = Product::all();
        return view('admin.product.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['units'] = Unit::all();
        $data['categories'] = Category::where('parent_id', 0)->get();
        $data['brands'] = Brand::all();
        $data['offers'] = Offer::all();
        return view('admin.product.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'brand_id' => 'required',
            'title' => 'required',
            'price' => 'required',
            'unit_id' => 'required',
            'image' => 'required|max:2048|mimes:jpg,png,jpeg',
        ]);

        $top_product = 0;
        if ($request->top_product) {
            $top_product = 1;
        }

        $data = array(
            'category_id' => $request->category_id,
            'brand_id' => $request->brand_id,
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'details' => $request->details,
            'features' => $request->features,
            'price' => $request->price,
            'special_price' => $request->special_price,
            'inventory_status' => $request->inventory_status,
            'inventory_stock' => $request->inventory_stock,
            'top_product' => $top_product,
            'unit_id' => $request->unit_id,
        );

        $product = Product::create($data);

        if ($request->hasFile('image')) {
            $image_name = $product->title . '-' . $product->id . '.' . $request->image->extension();
            $path = $request->image->move('uploads/product/', $image_name);

            $image_data = array(
                'product_id' => $product->id,
                'image' => $image_name,
            );
            Image::create($image_data);
        }

        if ($request->offers) {
            $offers = Offer::find($request->offers);
            $product->offers()->sync($offers);
        }
        return redirect()->route('products.index')->with('success', 'Product Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $units = Unit::all();
        $categories = Category::where('parent_id', 0)->get();
        $brands = Brand::all();
        $offers = Offer::all();
        return view('admin.product.edit', compact('product', 'units', 'categories', 'brands', 'offers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'title' => 'required',
            'price' => 'required',
            'unit_id' => 'required',
            'image' => 'max:2048|mimes:jpg,png,jpeg',
        ]);

        $top_product = 0;
        if ($request->top_product) {
            $top_product = 1;
        }

        $data = array(
            'category_id' => $request->category_id,
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'details' => $request->details,
            'features' => $request->features,
            'price' => $request->price,
            'special_price' => $request->special_price,
            'inventory_status' => $request->inventory_status,
            'inventory_stock' => $request->inventory_stock,
            'top_product' => $top_product,
            'unit_id' => $request->unit_id,
        );

        Product::where('id', $product->id)->update($data);

        if ($request->hasFile('image')) {
            $img = Image::where('product_id', $product->id)->first();
            if ($img->image && file_exists(public_path('uploads/product/' . $img->image))) {
                unlink(public_path('uploads/product/' . $img->image));
            }
            $image_name = $product->title . '-' . $product->id . '.' . $request->image->extension();
            $path = $request->image->move('uploads/product/', $image_name);

            $image_data = array(
                'product_id' => $product->id,
                'image' => $image_name,
            );
            Image::where('id', $img->id)->update($image_data);
        }

        if ($request->offers) {
            $offers = Offer::find($request->offers);
            $product->offers()->sync($offers);
        } else {
            $product->offers()->detach();
        }

        return redirect()->route('products.index')->with('success', 'Product Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if ($product) {
            $img = Image::where('product_id', $product->id)->first();
            if ($img->image) {
                $image = public_path('uploads/product/' . $img->image);
                if (file_exists($image)) {
                    unlink($image);
                }
            }
            $product->categories()->detach();
            if ($product->delete() && $img->delete()) {
                return redirect()->route('products.index')->with('success', 'product deleted.');
            } else {
                return redirect()->route('products.index')->with('error', 'Error while deleting product.');
            }
        } else {
            abort(404);
        }
    }

    public function showhide(Product $product)
    {
        if ($product->status) {
            $product->status = 0;
        } else {
            $product->status = 1;
        }
        $product->save();
        return redirect()->route('products.index')->with('success', 'Status Update.');
    }
}
