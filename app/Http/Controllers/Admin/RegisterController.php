<?php

namespace App\Http\Controllers\Admin;

use App\Register;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registers = Register::latest()->get();
        return view('admin.register.index', compact('registers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Register  $register
     * @return \Illuminate\Http\Response
     */
    public function show(Register $register)
    {
        return view('admin.register.show', compact('register'));
    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Register  $register
     * @return \Illuminate\Http\Response
     */
    public function destroy(Register $register)
    {
        if($register && $register->delete()){
            return redirect()->route('registers.index')->with('success', 'Register deleted.');
        }else{
            return redirect()->route('registers.index')->with('error', 'Error while deleting Register.');
        }
    }
}
