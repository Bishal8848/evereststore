<?php

namespace App\Http\Controllers\Admin;

use App\Register;
use App\Donate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        // $data['registers'] = Register::latest()->take(10)->get();
        // $data['donates'] = Donate::latest()->take(10)->get();
    	return view('admin.dashboard');
    }

    public function phpinfo()
    {
    	phpinfo();
    }

}
