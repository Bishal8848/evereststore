<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;

class CategoryController extends Controller
{

    public function index(Request $request)
    {
        if ($request->segment(1) == 'happy-home') {
            $category = Category::where('slug', $request->segment(2))->first();
        } else {
            $category = Category::where('slug', $request->segment(1))->first();
        }
        $products = Product::where('category_id', $category->id)->latest()->paginate(15);
        return view('category', compact('category', 'products'));
    }

    public function category($category)
    {
        $data = Category::where('slug', $category)->first();
        $products = Product::where('category_id', $data->id)->latest()->paginate(15);
        return view('product', compact('data', 'products'));
    }

    public function product($category, Product $product)
    {
        return view('single-product', compact('category', 'product'));
    }
}
