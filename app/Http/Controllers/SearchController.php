<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->keyword;
        $category = $request->category;
        $products = Product::where('title', 'like', '%' . $keyword . '%');
        if ($category) {
            $products->where('category_id', $category);
        }
        $products = $products->paginate(12);
        return view('search', compact('products', 'keyword'));
    }
}
