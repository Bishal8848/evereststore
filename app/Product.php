<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $attribute = [
		'inventory_status' => 0,
	];

	protected $fillable = [
		'category_id', 'brand_id', 'title', 'slug', 'image', 'details', 'features', 'price', 'special_price', 'unit_id', 'inventory_status', 'inventory_stock','top_product',
	];

	public function images()
	{
		return $this->hasMany('App\Image');
	}

	public function getProductThumbAttribute()
	{
		$image = $this->images ? $this->images[0]->image : null;
		return $image;
	}

	public function category()
	{
		return $this->belongsTo('App\Category');
	}

	public function brand()
	{
		return $this->belongsTo('App\Brand');
	}

	public function unit()
	{
		return $this->belongsTo('App\Unit');
	}

	public function offers()
	{
		return $this->belongsToMany(Offer::class,'offer_products');
	}
}
