<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	public function scopeOfValue($query, $title)
	{
		$setting = $query->where('title', $title)->first();
		return $setting ? ($setting->value ? $setting->value : '&nbsp;') : $title;
	}
}
