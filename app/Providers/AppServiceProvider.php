<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('main_menu', \App\Menu::main());
            $view->with('siteID', \App\Site::ID());
            $view->with('footer_menu', \App\Menu::footer());
            $view->with('socials', \App\Social::all());
            $view->with('menu_categories', \App\Category::where('store_id', \App\Site::ID())->where('parent_id', 0)->orderBy('order_by')->get());
            $view->with('footer_content', \App\Page::where('slug','=','footer-content')->first());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
