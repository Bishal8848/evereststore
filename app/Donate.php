<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donate extends Model
{
    protected $attributes = [
		'status' => 0,
	];

	public function getNameAttribute()
	{
		return "{$this->first_name} {$this->last_name}";
		
	}
	
}
