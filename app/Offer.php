<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
    	'title', 'details', 'start_date', 'end_date',
    ];

    public function products()
    {
    	return $this->belongsToMany(Product::class,'offer_products');
    }
}
