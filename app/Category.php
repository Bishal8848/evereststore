<?php

namespace App;

use App\Base;

class Category extends Base
{
	protected $fillable = [
		'store_id', 'parent_id', 'title', 'slug', 'image', 'details', 'show_in_menu', 'meta_title', 'meta_description', 'meta_keyword', 'order_by'
	];

	protected $file_path = 'category';

	public function parent()
	{
		return $this->belongsTo('App\Category', 'parent_id');
	}

	public function subcats()
	{
		return $this->hasMany('App\Category', 'parent_id');
	}

	public function products()
	{
		return $this->hasMany(Product::class);
	}

	public function getUrlAttribute()
	{
		return url($this->store_id == 2 ? 'happy-home/' .$this->slug:$this->slug);
	}
}
