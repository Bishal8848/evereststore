<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
	public static function ID()
	{
		$segment = \Request::segment(1);

		if ($segment == 'happy-home') {
			return 2;
		}
		return 1;
	}
}
