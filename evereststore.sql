-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2019 at 07:10 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evereststore`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `store_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `show_in_menu` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `meta_title` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keyword` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `store_id`, `parent_id`, `title`, `slug`, `image`, `details`, `show_in_menu`, `status`, `meta_title`, `meta_description`, `meta_keyword`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'Kitchen', 'kitchen', NULL, 'Kitchen', 0, 1, NULL, NULL, NULL, '2019-02-11 00:25:28', '2019-02-11 00:26:22'),
(2, 1, 0, 'Household', 'household', NULL, NULL, 0, 1, NULL, NULL, NULL, '2019-02-11 00:26:42', '2019-02-11 00:26:42'),
(3, 1, 0, 'Snacks & Beverages', 'snacks-beverages', NULL, NULL, 0, 1, NULL, NULL, NULL, '2019-02-11 00:27:10', '2019-02-11 00:27:10'),
(4, 1, 0, 'Personal Care', 'personal-care', NULL, NULL, 0, 1, NULL, NULL, NULL, '2019-02-11 00:48:00', '2019-02-11 00:48:00'),
(5, 1, 0, 'Gift Hampers', 'gift-hampers', NULL, NULL, 0, 1, NULL, NULL, NULL, '2019-02-11 00:48:15', '2019-02-11 00:48:15'),
(6, 1, 0, 'Fruits & Vegetables', 'fruits-vegetables', NULL, NULL, 0, 1, NULL, NULL, NULL, '2019-02-11 00:48:34', '2019-02-11 00:48:34'),
(7, 1, 0, 'Baby Care', 'baby-care', NULL, NULL, 0, 1, NULL, NULL, NULL, '2019-02-11 00:48:48', '2019-02-11 00:48:48'),
(8, 1, 0, 'Soft Drinks & Juices', 'soft-drinks-juices', NULL, NULL, 0, 1, NULL, NULL, NULL, '2019-02-11 00:49:08', '2019-02-11 00:49:08'),
(9, 1, 0, 'Frozen Food', 'frozen-food', NULL, NULL, 0, 1, NULL, NULL, NULL, '2019-02-11 00:49:22', '2019-02-11 00:49:22'),
(10, 1, 0, 'Bread & Bakery', 'bread-bakery', NULL, NULL, 0, 1, NULL, NULL, NULL, '2019-02-11 00:49:39', '2019-02-11 00:49:39'),
(11, 1, 0, 'Sweets', 'sweets', NULL, NULL, 0, 1, NULL, NULL, NULL, '2019-02-11 00:49:52', '2019-02-11 00:49:52');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `code` varchar(2) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `sprite` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`, `sprite`) VALUES
(1, 'AF', 'Afghanistan', NULL),
(2, 'AL', 'Albania', NULL),
(3, 'DZ', 'Algeria', NULL),
(4, 'DS', 'American Samoa', NULL),
(5, 'AD', 'Andorra', NULL),
(6, 'AO', 'Angola', NULL),
(7, 'AI', 'Anguilla', NULL),
(8, 'AQ', 'Antarctica', NULL),
(9, 'AG', 'Antigua and Barbuda', NULL),
(10, 'AR', 'Argentina', NULL),
(11, 'AM', 'Armenia', NULL),
(12, 'AW', 'Aruba', NULL),
(13, 'AU', 'Australia', NULL),
(14, 'AT', 'Austria', NULL),
(15, 'AZ', 'Azerbaijan', NULL),
(16, 'BS', 'Bahamas', NULL),
(17, 'BH', 'Bahrain', NULL),
(18, 'BD', 'Bangladesh', NULL),
(19, 'BB', 'Barbados', NULL),
(20, 'BY', 'Belarus', NULL),
(21, 'BE', 'Belgium', NULL),
(22, 'BZ', 'Belize', NULL),
(23, 'BJ', 'Benin', NULL),
(24, 'BM', 'Bermuda', NULL),
(25, 'BT', 'Bhutan', NULL),
(26, 'BO', 'Bolivia', NULL),
(27, 'BA', 'Bosnia and Herzegovina', NULL),
(28, 'BW', 'Botswana', NULL),
(29, 'BV', 'Bouvet Island', NULL),
(30, 'BR', 'Brazil', NULL),
(31, 'IO', 'British Indian Ocean Territory', NULL),
(32, 'BN', 'Brunei Darussalam', NULL),
(33, 'BG', 'Bulgaria', NULL),
(34, 'BF', 'Burkina Faso', NULL),
(35, 'BI', 'Burundi', NULL),
(36, 'KH', 'Cambodia', NULL),
(37, 'CM', 'Cameroon', NULL),
(38, 'CA', 'Canada', NULL),
(39, 'CV', 'Cape Verde', NULL),
(40, 'KY', 'Cayman Islands', NULL),
(41, 'CF', 'Central African Republic', NULL),
(42, 'TD', 'Chad', NULL),
(43, 'CL', 'Chile', NULL),
(44, 'CN', 'China', NULL),
(45, 'CX', 'Christmas Island', NULL),
(46, 'CC', 'Cocos (Keeling) Islands', NULL),
(47, 'CO', 'Colombia', NULL),
(48, 'KM', 'Comoros', NULL),
(49, 'CG', 'Congo', NULL),
(50, 'CK', 'Cook Islands', NULL),
(51, 'CR', 'Costa Rica', NULL),
(52, 'HR', 'Croatia (Hrvatska)', NULL),
(53, 'CU', 'Cuba', NULL),
(54, 'CY', 'Cyprus', NULL),
(55, 'CZ', 'Czech Republic', NULL),
(56, 'DK', 'Denmark', NULL),
(57, 'DJ', 'Djibouti', NULL),
(58, 'DM', 'Dominica', NULL),
(59, 'DO', 'Dominican Republic', NULL),
(60, 'TP', 'East Timor', NULL),
(61, 'EC', 'Ecuador', NULL),
(62, 'EG', 'Egypt', NULL),
(63, 'SV', 'El Salvador', NULL),
(64, 'GQ', 'Equatorial Guinea', NULL),
(65, 'ER', 'Eritrea', NULL),
(66, 'EE', 'Estonia', NULL),
(67, 'ET', 'Ethiopia', NULL),
(68, 'FK', 'Falkland Islands (Malvinas)', NULL),
(69, 'FO', 'Faroe Islands', NULL),
(70, 'FJ', 'Fiji', NULL),
(71, 'FI', 'Finland', NULL),
(72, 'FR', 'France', NULL),
(73, 'FX', 'France, Metropolitan', NULL),
(74, 'GF', 'French Guiana', NULL),
(75, 'PF', 'French Polynesia', NULL),
(76, 'TF', 'French Southern Territories', NULL),
(77, 'GA', 'Gabon', NULL),
(78, 'GM', 'Gambia', NULL),
(79, 'GE', 'Georgia', NULL),
(80, 'DE', 'Germany', NULL),
(81, 'GH', 'Ghana', NULL),
(82, 'GI', 'Gibraltar', NULL),
(83, 'GK', 'Guernsey', NULL),
(84, 'GR', 'Greece', NULL),
(85, 'GL', 'Greenland', NULL),
(86, 'GD', 'Grenada', NULL),
(87, 'GP', 'Guadeloupe', NULL),
(88, 'GU', 'Guam', NULL),
(89, 'GT', 'Guatemala', NULL),
(90, 'GN', 'Guinea', NULL),
(91, 'GW', 'Guinea-Bissau', NULL),
(92, 'GY', 'Guyana', NULL),
(93, 'HT', 'Haiti', NULL),
(94, 'HM', 'Heard and Mc Donald Islands', NULL),
(95, 'HN', 'Honduras', NULL),
(96, 'HK', 'Hong Kong', NULL),
(97, 'HU', 'Hungary', NULL),
(98, 'IS', 'Iceland', NULL),
(99, 'IN', 'India', NULL),
(100, 'IM', 'Isle of Man', NULL),
(101, 'ID', 'Indonesia', NULL),
(102, 'IR', 'Iran (Islamic Republic of)', NULL),
(103, 'IQ', 'Iraq', NULL),
(104, 'IE', 'Ireland', NULL),
(105, 'IL', 'Israel', NULL),
(106, 'IT', 'Italy', NULL),
(107, 'CI', 'Ivory Coast', NULL),
(108, 'JE', 'Jersey', NULL),
(109, 'JM', 'Jamaica', NULL),
(110, 'JP', 'Japan', NULL),
(111, 'JO', 'Jordan', NULL),
(112, 'KZ', 'Kazakhstan', NULL),
(113, 'KE', 'Kenya', NULL),
(114, 'KI', 'Kiribati', NULL),
(115, 'KP', 'Korea, Democratic People\'s Republic of', NULL),
(116, 'KR', 'Korea, Republic of', NULL),
(117, 'XK', 'Kosovo', NULL),
(118, 'KW', 'Kuwait', NULL),
(119, 'KG', 'Kyrgyzstan', NULL),
(120, 'LA', 'Lao People\'s Democratic Republic', NULL),
(121, 'LV', 'Latvia', NULL),
(122, 'LB', 'Lebanon', NULL),
(123, 'LS', 'Lesotho', NULL),
(124, 'LR', 'Liberia', NULL),
(125, 'LY', 'Libyan Arab Jamahiriya', NULL),
(126, 'LI', 'Liechtenstein', NULL),
(127, 'LT', 'Lithuania', NULL),
(128, 'LU', 'Luxembourg', NULL),
(129, 'MO', 'Macau', NULL),
(130, 'MK', 'Macedonia', NULL),
(131, 'MG', 'Madagascar', NULL),
(132, 'MW', 'Malawi', NULL),
(133, 'MY', 'Malaysia', NULL),
(134, 'MV', 'Maldives', NULL),
(135, 'ML', 'Mali', NULL),
(136, 'MT', 'Malta', NULL),
(137, 'MH', 'Marshall Islands', NULL),
(138, 'MQ', 'Martinique', NULL),
(139, 'MR', 'Mauritania', NULL),
(140, 'MU', 'Mauritius', NULL),
(141, 'TY', 'Mayotte', NULL),
(142, 'MX', 'Mexico', NULL),
(143, 'FM', 'Micronesia, Federated States of', NULL),
(144, 'MD', 'Moldova, Republic of', NULL),
(145, 'MC', 'Monaco', NULL),
(146, 'MN', 'Mongolia', NULL),
(147, 'ME', 'Montenegro', NULL),
(148, 'MS', 'Montserrat', NULL),
(149, 'MA', 'Morocco', NULL),
(150, 'MZ', 'Mozambique', NULL),
(151, 'MM', 'Myanmar', NULL),
(152, 'NA', 'Namibia', NULL),
(153, 'NR', 'Nauru', NULL),
(154, 'NP', 'Nepal', NULL),
(155, 'NL', 'Netherlands', NULL),
(156, 'AN', 'Netherlands Antilles', NULL),
(157, 'NC', 'New Caledonia', NULL),
(158, 'NZ', 'New Zealand', NULL),
(159, 'NI', 'Nicaragua', NULL),
(160, 'NE', 'Niger', NULL),
(161, 'NG', 'Nigeria', NULL),
(162, 'NU', 'Niue', NULL),
(163, 'NF', 'Norfolk Island', NULL),
(164, 'MP', 'Northern Mariana Islands', NULL),
(165, 'NO', 'Norway', NULL),
(166, 'OM', 'Oman', NULL),
(167, 'PK', 'Pakistan', NULL),
(168, 'PW', 'Palau', NULL),
(169, 'PS', 'Palestine', NULL),
(170, 'PA', 'Panama', NULL),
(171, 'PG', 'Papua New Guinea', NULL),
(172, 'PY', 'Paraguay', NULL),
(173, 'PE', 'Peru', NULL),
(174, 'PH', 'Philippines', NULL),
(175, 'PN', 'Pitcairn', NULL),
(176, 'PL', 'Poland', NULL),
(177, 'PT', 'Portugal', NULL),
(178, 'PR', 'Puerto Rico', NULL),
(179, 'QA', 'Qatar', NULL),
(180, 'RE', 'Reunion', NULL),
(181, 'RO', 'Romania', NULL),
(182, 'RU', 'Russian Federation', NULL),
(183, 'RW', 'Rwanda', NULL),
(184, 'KN', 'Saint Kitts and Nevis', NULL),
(185, 'LC', 'Saint Lucia', NULL),
(186, 'VC', 'Saint Vincent and the Grenadines', NULL),
(187, 'WS', 'Samoa', NULL),
(188, 'SM', 'San Marino', NULL),
(189, 'ST', 'Sao Tome and Principe', NULL),
(190, 'SA', 'Saudi Arabia', NULL),
(191, 'SN', 'Senegal', NULL),
(192, 'RS', 'Serbia', NULL),
(193, 'SC', 'Seychelles', NULL),
(194, 'SL', 'Sierra Leone', NULL),
(195, 'SG', 'Singapore', NULL),
(196, 'SK', 'Slovakia', NULL),
(197, 'SI', 'Slovenia', NULL),
(198, 'SB', 'Solomon Islands', NULL),
(199, 'SO', 'Somalia', NULL),
(200, 'ZA', 'South Africa', NULL),
(201, 'GS', 'South Georgia South Sandwich Islands', NULL),
(202, 'ES', 'Spain', NULL),
(203, 'LK', 'Sri Lanka', NULL),
(204, 'SH', 'St. Helena', NULL),
(205, 'PM', 'St. Pierre and Miquelon', NULL),
(206, 'SD', 'Sudan', NULL),
(207, 'SR', 'Suriname', NULL),
(208, 'SJ', 'Svalbard and Jan Mayen Islands', NULL),
(209, 'SZ', 'Swaziland', NULL),
(210, 'SE', 'Sweden', NULL),
(211, 'CH', 'Switzerland', NULL),
(212, 'SY', 'Syrian Arab Republic', NULL),
(213, 'TW', 'Taiwan', NULL),
(214, 'TJ', 'Tajikistan', NULL),
(215, 'TZ', 'Tanzania, United Republic of', NULL),
(216, 'TH', 'Thailand', NULL),
(217, 'TG', 'Togo', NULL),
(218, 'TK', 'Tokelau', NULL),
(219, 'TO', 'Tonga', NULL),
(220, 'TT', 'Trinidad and Tobago', NULL),
(221, 'TN', 'Tunisia', NULL),
(222, 'TR', 'Turkey', NULL),
(223, 'TM', 'Turkmenistan', NULL),
(224, 'TC', 'Turks and Caicos Islands', NULL),
(225, 'TV', 'Tuvalu', NULL),
(226, 'UG', 'Uganda', NULL),
(227, 'UA', 'Ukraine', NULL),
(228, 'AE', 'United Arab Emirates', NULL),
(229, 'GB', 'United Kingdom', NULL),
(230, 'US', 'United States', NULL),
(231, 'UM', 'United States minor outlying islands', NULL),
(232, 'UY', 'Uruguay', NULL),
(233, 'UZ', 'Uzbekistan', NULL),
(234, 'VU', 'Vanuatu', NULL),
(235, 'VA', 'Vatican City State', NULL),
(236, 'VE', 'Venezuela', NULL),
(237, 'VN', 'Vietnam', NULL),
(238, 'VG', 'Virgin Islands (British)', NULL),
(239, 'VI', 'Virgin Islands (U.S.)', NULL),
(240, 'WF', 'Wallis and Futuna Islands', NULL),
(241, 'EH', 'Western Sahara', NULL),
(242, 'YE', 'Yemen', NULL),
(243, 'ZR', 'Zaire', NULL),
(244, 'ZM', 'Zambia', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `product_id`, `image`, `caption`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Kitchen One-1.jpeg', NULL, 1, '2019-02-11 02:49:28', '2019-02-11 02:49:28'),
(2, 2, 'Est reprehenderit ne-2.jpeg', NULL, 1, '2019-02-11 02:49:54', '2019-02-11 02:49:54'),
(3, 3, 'Architecto fugiat ex-3.jpeg', NULL, 1, '2019-02-11 02:50:09', '2019-02-11 02:50:09'),
(4, 4, 'Recusandae Fugiat-4.jpeg', NULL, 1, '2019-02-11 02:50:25', '2019-02-11 02:50:25'),
(5, 5, 'Culpa maxime velit-5.jpeg', NULL, 1, '2019-02-13 00:32:27', '2019-02-13 00:32:27'),
(6, 6, 'Corrupti omnis inve-6.jpeg', NULL, 1, '2019-02-13 00:32:41', '2019-02-13 00:32:41'),
(7, 7, 'Non quo voluptatem-7.jpeg', NULL, 1, '2019-02-13 00:32:56', '2019-02-13 00:32:56');

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `id` int(10) UNSIGNED NOT NULL,
  `page` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labelid` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`id`, `page`, `labelid`, `value`, `created_at`, `updated_at`) VALUES
(1, 'home', 'home:welcome_title', 'Welcome to Arc and You', '2018-10-07 05:03:56', '2018-10-07 05:03:56'),
(2, 'home', 'home:welcome_sub', 'let\'s do something for social', '2018-10-07 05:04:18', '2018-10-07 05:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2019_02_04_094636_create_units_table', 2),
(8, '2019_02_04_070914_create_categories_table', 3),
(9, '2019_02_04_094910_create_products_table', 3),
(10, '2019_02_04_105026_create_images_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overview` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `order_by` int(11) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `footer_order` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci,
  `meta_keyword` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `title`, `slug`, `overview`, `details`, `status`, `order_by`, `menu_order`, `footer_order`, `position`, `meta_title`, `meta_keyword`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 0, 'Home', '/', NULL, '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 1, NULL, NULL, NULL, 3, NULL, NULL, NULL, '2019-02-11 00:55:25', '2019-02-11 00:55:25'),
(2, 0, 'About Us', 'about-us', NULL, '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-02-11 00:56:08', '2019-02-11 00:56:08'),
(3, 0, 'Privacy Policy', 'privacy-policy', NULL, '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-02-11 00:56:26', '2019-02-11 00:56:26'),
(4, 0, 'FAQs', 'faqs', NULL, '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-02-11 00:56:39', '2019-02-11 00:56:39'),
(5, 0, 'Contact', 'contact', NULL, '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2019-02-11 00:57:01', '2019-02-11 00:57:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('kml.pandey77@gmail.com', '$2y$10$VogLeuvmjuUHKjGHkUfI/uNpaX4ag4W5ggxxSJgLTqkDtqaFvn87u', '2018-09-24 10:25:47');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `features` text COLLATE utf8mb4_unicode_ci,
  `price` double(8,2) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `title`, `slug`, `details`, `features`, `price`, `unit_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Kitchen One', 'kitchen-one', NULL, 'Sapiente eu sit quis', 978.00, 2, 1, '2019-02-11 02:49:28', '2019-02-11 02:49:28'),
(2, 1, 'Est reprehenderit ne', 'est-reprehenderit-ne', NULL, 'Irure sed dignissimo', 112.00, 2, 1, '2019-02-11 02:49:54', '2019-02-11 02:49:54'),
(3, 1, 'Architecto fugiat ex', 'architecto-fugiat-ex', NULL, 'Voluptatum omnis cup', 520.00, 1, 1, '2019-02-11 02:50:09', '2019-02-11 02:50:09'),
(4, 1, 'Recusandae Fugiat', 'recusandae-fugiat', NULL, 'Quae omnis in quisqu', 905.00, 1, 1, '2019-02-11 02:50:25', '2019-02-11 02:50:25'),
(5, 2, 'Culpa maxime velit', 'culpa-maxime-velit', '<p>asdfasdfadf</p>', 'Ex ad dolore necessi', 263.00, 2, 1, '2019-02-13 00:32:27', '2019-02-13 00:32:27'),
(6, 3, 'Corrupti omnis inve', 'corrupti-omnis-inve', '<p>sdafasdf</p>', 'Pariatur Corrupti', 718.00, 2, 1, '2019-02-13 00:32:41', '2019-02-13 00:32:41'),
(7, 4, 'Non quo voluptatem', 'non-quo-voluptatem', '<p>hfjdfgh</p>', 'Ut alias beatae iust', 90.00, 1, 1, '2019-02-13 00:32:56', '2019-02-13 00:32:56');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_id` int(11) DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `event_date` date DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `package_id`, `title`, `name`, `email`, `country_id`, `image`, `details`, `rating`, `event_date`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'John Frank', NULL, NULL, 'review-1.jpeg', '<p>Nam Cumque nihil impedit quo minuslibero tempore, nihil impedit quo minus id quod possimus, Nam Cumque nihil impedit quo minuslibero tempore, cum soluta nobis est eligendi optio cumque nihil impedit omnis voluptas</p>', NULL, NULL, 1, '2018-10-09 00:47:58', '2018-10-09 00:47:58'),
(2, NULL, NULL, 'John Doe', NULL, NULL, 'review-2.jpeg', '<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>', NULL, NULL, 1, '2018-10-09 00:49:13', '2018-10-09 00:49:21');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `value`, `created_at`, `updated_at`) VALUES
(1, 'emails', 'admin@arcandyou.com;', '2018-10-07 05:01:33', '2018-10-07 05:02:02');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `link` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL,
  `order_by` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `package_id`, `title`, `details`, `image`, `link`, `status`, `order_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'Donation for the Poor Children', '<p>Raise your helping hand for poor people.</p>', 'donation-for-the-poor-children-1.jpeg', NULL, 1, 1, '2018-01-24 01:33:02', '2018-10-08 23:34:46'),
(2, 0, 'Stand Together', 'Be a part of the breakthrough and make someone’s dream come true.', 'stand-together-2.jpeg', NULL, 1, 2, '2018-01-24 01:33:18', '2018-10-09 00:02:04');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

CREATE TABLE `socials` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `title`, `icon`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Facebook', 'facebook-f', 'https://www.facebook.com/arcandyou', 1, '2018-04-04 22:38:53', '2018-10-09 05:37:04'),
(2, 'Twitter', 'twitter', 'http://twitter.com/arcandyou', 1, '2018-10-09 05:37:33', '2018-10-09 05:38:20');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Kg', '2019-02-04 04:21:55', '2019-02-04 04:21:55'),
(2, 'Pcs', '2019-02-04 04:23:34', '2019-02-04 04:23:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `groups` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `groups`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 100, 'admin', 'admin@evereststore.com', '$2y$10$s.Lv8qz510BzQG1jicqFwuDV3H0SvRxJR5n9bCuXxZEWJG6C8qNYK', '2ofoH5IA7o3qPi6A2PDR4r1lrTptBTyuD1BGDXjZEcgOiq3tcm3Lsu8C9bnT', '2018-01-09 00:54:37', '2018-01-24 01:19:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(191)),
  ADD KEY `name_2` (`name`(191));

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=245;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `socials`
--
ALTER TABLE `socials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
